﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject
{
    public partial class ViewStudents : Form
    {
        int ID, Gender;
        string RegNo, FN, LN, Contact, DOB, Email;
        public ViewStudents()
        {
            InitializeComponent();
            dataGridView1.ReadOnly = true;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Std.Id , Std.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.DateOfBirth , P.Email , LU.Value as Gender \r\nfrom Student as Std\r\njoin Person as P\r\n on Std.Id = P.Id \r\n join Lookup as LU \r\non LU.Id = P.Gender");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
        private void siticoneButton2_Click(object sender, EventArgs e)
        {
            if (FN == null || LN == null || Email == null || Contact == null)
            {
                MessageBox.Show("Select a record");
            }
            if (FN != null || LN != null || Email != null || Contact != null)
            {
                UpdateStudent f = new UpdateStudent(ID, RegNo, FN, LN, Contact,DOB,Email,Gender);
                this.Hide();
                f.Show();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Student s = new Student();
            s.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {
            Student s = new Student();
            s.Show();
            this.Hide();
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            RegNo = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            FN = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            LN = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
            Contact = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            DOB = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
            Email = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();

            if (dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString() == "Male")
            {
                Gender = 1;
            }
            else if (dataGridView1.Rows[e.RowIndex].Cells[7].Value.ToString() == "Female")
            {
                Gender = 2;
            }

        }

        private void EvaluationsBtnM_Click_1(object sender, EventArgs e)
        {
            Evaluation E = new Evaluation();
            E.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {
            Advisor A = new Advisor();
            A.Show();
            this.Hide();
        }
        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {
            Project p = new Project();
            p.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {
            Groups g = new Groups();
            g.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {
            Evaluation E = new Evaluation();
            E.Show();
            this.Hide();
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {

            dataGridView1.ReadOnly = true;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Std.Id , Std.RegistrationNo , P.FirstName , P.LastName , P.Contact , P.DateOfBirth , P.Email , LU.Value as Gender \r\nfrom Student as Std\r\njoin Person as P\r\n on Std.Id = P.Id \r\n join Lookup as LU \r\non LU.Id = P.Gender");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }



        private void siticoneButton3_Click(object sender, EventArgs e)
        {

            if (ID != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Delete From Student Where Id = @Id");
                    cmd.Parameters.AddWithValue("@Id", ID);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");


                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }

            }

            else
            {
                MessageBox.Show("Select a record to delete");
            }
        }
    }
}
