﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject
{
    public partial class UpdateStudent : Form
    {
        int Id_text;
        public UpdateStudent()
        {
            InitializeComponent();

        
        }
        public UpdateStudent(int ID1, string RegNO1, string FN1, string LN1, string Contact1, string DOB1, string Email1, int Gender1)
        {
            InitializeComponent();
            Gender.Items.Add("Male");
            Gender.Items.Add("Female");
            Id_text = ID1;
            RegNo.Text = RegNO1;
            Fname.Text = FN1;
            Lname.Text = LN1;
            contact.Text = Contact1;
            dateTimePicker1.Value = DateTime.Parse(DOB1);
            email.Text = Email1;
            Gender.SelectedIndex = Gender1 - 1;

        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
                if (Id_text != 0)
                {
                    try
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("update Person set FirstName = @FirstName, LastName = @LastName, Contact = @Contact, Email = @Email, DateOfBirth = @DOB, Gender = @Gender where Id = @ID \r\n update Student set RegistrationNo = @RegistrationNumber where Id = @ID ", con);
                        cmd.Parameters.AddWithValue("@FirstName", Fname.Text);
                        cmd.Parameters.AddWithValue("@LastName", Lname.Text);
                        cmd.Parameters.AddWithValue("@Contact", contact.Text);
                        cmd.Parameters.AddWithValue("@Email", email.Text);
                        cmd.Parameters.AddWithValue("@ID", Id_text);
                        cmd.Parameters.AddWithValue("@RegistrationNumber", RegNo.Text);
                        cmd.Parameters.AddWithValue("@Gender", Gender.SelectedIndex + 1);
                        dateTimePicker1.Format = DateTimePickerFormat.Custom;
                        dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                        cmd.Parameters.AddWithValue("@DOB", dateTimePicker1.Text);
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Successfully updated");

                        Student f = new Student();
                        this.Hide();
                        f.Show();
                    }

                    catch (Exception error)
                    {
                        MessageBox.Show(error.Message);
                    }

                }
            }

            private void MenuBtnM_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Student s = new Student();
            s.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Student s = new Student();
            s.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Advisor A = new Advisor();
            A.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Project g = new Project();
            g.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Groups g = new Groups();
            g.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Evaluation E = new Evaluation();
            E.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
