﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject
{
    public partial class AddStudent : Form
    {
        public AddStudent()
        {
            InitializeComponent();
            Gender.Items.Add("Male");
            Gender.Items.Add("Female");

        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            bool check = true;
            int gender = -1;

            if (check == true)
            {
                if (string.IsNullOrEmpty(RegNo.Text) || string.IsNullOrEmpty(Fname.Text) || string.IsNullOrEmpty(Lname.Text) || string.IsNullOrEmpty(contact.Text) || string.IsNullOrEmpty(email.Text))
                {
                    check = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            try
            {
                if (check == true)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO Person(FirstName,LastName,Contact,Email,DateOfBirth,Gender) VALUES (@FirstName,@LastName, @Contact,@Email,@DOB, @Gender)", con);
                    SqlCommand cmd1 = new SqlCommand("INSERT INTO Student(Id,RegistrationNo) VALUES ((SELECT Id FROM Person WHERE FirstName = @FirstName AND LastName=@LastName AND Contact=@Contact AND Email=@Email AND DateOfBirth=@DOB AND Gender=@Gender) ,@RegistrationNo)", con);
                    cmd.Parameters.AddWithValue("@FirstName", Fname.Text);
                    cmd.Parameters.AddWithValue("@LastName", Lname.Text);
                    cmd.Parameters.AddWithValue("@Contact", contact.Text);
                    cmd.Parameters.AddWithValue("@Email", email.Text);
                    cmd1.Parameters.AddWithValue("@FirstName", Fname.Text);
                    cmd1.Parameters.AddWithValue("@LastName", Lname.Text);
                    cmd1.Parameters.AddWithValue("@Contact", contact.Text);
                    cmd1.Parameters.AddWithValue("@Email", email.Text);
                    cmd1.Parameters.AddWithValue("@RegistrationNo", RegNo.Text);

                    if (Gender.SelectedItem == "Male")
                    {
                        gender = 1;
                    }
                    if (Gender.SelectedItem == "Female")
                    {
                        gender = 2;
                    }

                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@Gender", gender);
                    cmd.Parameters.AddWithValue("@DOB", dateTimePicker1.Text);
                    cmd1.Parameters.AddWithValue("@Gender", gender);
                    cmd1.Parameters.AddWithValue("@DOB", dateTimePicker1.Text);
                    cmd.ExecuteNonQuery();
                    cmd1.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");

                    Student s = new Student();
                    s.Show();
                    this.Hide();
                }
            }
            catch( Exception Error)
            {
                MessageBox.Show(Error.Message);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Student s = new Student();
            s.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Student s = new Student();
            s.Show();
            this.Hide();

        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {
            Advisor A = new Advisor();
            A.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {
            Project p = new Project();
            p.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {
            Groups g = new Groups();
            g.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {
            Evaluation E = new Evaluation();
            E.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
