﻿using midtermProject.Evaluations;
using midtermProject.Group;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void MenuBtnM_Click(object sender, EventArgs e)
        {
        }


        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            Student s = new Student();
            s.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Student s = new Student();
            s.Show();
            this.Hide();
        }

        private void siticoneButton2_Click(object sender, EventArgs e)
        {
            Advisor a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Advisor a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void siticoneButton3_Click(object sender, EventArgs e)
        {
            Project p = new Project();
            p.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Project p = new Project();
            p.Show();
            this.Hide();
        }

        private void siticoneButton4_Click(object sender, EventArgs e)
        {
            Groups g = new Groups();
            g.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form g = new Groups();
            g.Show();
            this.Hide();
        }


        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form E = new Evaluation();
            E.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {
            Form1 H = new Form1();
            H.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
            
        }

        private void siticoneButton5_Click(object sender, EventArgs e)
        {
            Form a = new Evaluation();           
            a.Show();
            this.Hide();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void MarksBtnM_Click(object sender, EventArgs e)
        {

            String connection = @"Data Source=(local);Initial Catalog=ProjectADatabase;Integrated Security=True";
            string query = "select S.RegistrationNo, PR.FirstName, PR.LastName, G.Id as [Group Id], P.Title as [Project Title], E.TotalMarks, GE.ObtainedMarks from GroupEvaluation GE join Evaluation E on GE.EvaluationId = E.Id  join [Group] G on G.Id = GE.GroupId join GroupStudent GS on GS.GroupId = G.Id join Student S on S.Id = GS.StudentId join GroupProject GP on GP.GroupId = GS.GroupId join Project P on P.Id = GP.ProjectId join Person PR on PR.Id = S.Id";
            PDF.GenerateMarksheet(connection, query);

            MessageBox.Show("PDF Report has been generated");
        }
    }
}
