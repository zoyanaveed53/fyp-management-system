﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Advisors
{
    public partial class AddAdvisor : Form
    {
        public AddAdvisor()
        {
            InitializeComponent();
            DesignationBox.Items.Add("Professor");
            DesignationBox.Items.Add("Assisstant Professor");
            DesignationBox.Items.Add("Associate Professor");
            DesignationBox.Items.Add("Lecturer");
            DesignationBox.Items.Add("Industry Professional");
            Gender.Items.Add("Male");
            Gender.Items.Add("Female");

        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            bool check = true;
            int gender = -1;

            if (check == true)
            {
                if (string.IsNullOrEmpty(Salary.Text) || string.IsNullOrEmpty(Fname.Text) || string.IsNullOrEmpty(Lname.Text) || string.IsNullOrEmpty(contact.Text) || string.IsNullOrEmpty(email.Text))
                {
                    check = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            try
            {
                if (check == true)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO Person (FirstName, LastName, Contact, Email, DateOfBirth, Gender) VALUES (@FirstName,@LastName, @Contact,@Email,@DOB, @Gender)", con);
                    SqlCommand cmd1 = new SqlCommand("INSERT INTO Advisor (Id, Designation, Salary) VALUES ((SELECT Id FROM Person WHERE FirstName = @FirstName AND LastName=@LastName AND Contact=@Contact AND Email=@Email AND DateOfBirth=@DOB AND Gender=@Gender), @Designation, @Salary)", con);
                    cmd.Parameters.AddWithValue("@FirstName", Fname.Text);
                    cmd.Parameters.AddWithValue("@LastName", Lname.Text);
                    cmd.Parameters.AddWithValue("@Contact", contact.Text);
                    cmd.Parameters.AddWithValue("@Email", email.Text);
                    cmd1.Parameters.AddWithValue("@FirstName", Fname.Text);
                    cmd1.Parameters.AddWithValue("@LastName", Lname.Text);
                    cmd1.Parameters.AddWithValue("@Contact", contact.Text);
                    cmd1.Parameters.AddWithValue("@Email", email.Text);
                    cmd1.Parameters.AddWithValue("@Salary", Salary.Text);
                    cmd1.Parameters.AddWithValue("@Designation", DesignationBox.SelectedIndex + 6);

                    if (Gender.SelectedItem == "Male")
                    {
                        gender = 1;
                    }
                    if (Gender.SelectedItem == "Female")
                    {
                        gender = 2;
                    }

                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@Gender", gender);
                    cmd.Parameters.AddWithValue("@DOB", dateTimePicker1.Text);
                    cmd1.Parameters.AddWithValue("@Gender", gender);
                    cmd1.Parameters.AddWithValue("@DOB", dateTimePicker1.Text);

                    cmd.ExecuteNonQuery();
                    cmd1.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");
                    this.Hide();
                    Advisor f = new Advisor();
                    f.Show();
                }
            }

            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Advisor s = new Advisor();
            s.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Student s = new Student();
            s.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Advisor A = new Advisor();
            A.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Project g = new Project();
            g.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Groups g = new Groups();
            g.Show();
            this.Hide();
        }


        private void HomeBtnM_Click_1(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click_1(object sender, EventArgs e)
        {
            Evaluation E = new Evaluation();
            E.Show();
            this.Hide();

        }

        private void StudentsBtnM_Click_1(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click_1(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click_1(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click_1(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {
            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
