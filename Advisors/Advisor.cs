﻿using midtermProject.Advisors;
using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject
{
    public partial class Advisor : Form
    {
        public Advisor()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {
            Student s = new Student();
            s.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You're Already on the page");
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {
            Project p = new Project();
            p.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {
            Groups g = new Groups();
            g.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {
            Evaluation E = new Evaluation();
            E.Show();
            this.Hide();
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            AddAdvisor a = new AddAdvisor();
            a.Show();
            this.Hide();
        }

        private void siticoneButton2_Click(object sender, EventArgs e)
        {
            ViewAdvisor v = new ViewAdvisor();
            v.Show();
            this.Hide();
        }

        private void siticoneButton3_Click(object sender, EventArgs e)
        {
            UpdateAdvisor1 u = new UpdateAdvisor1();
            u.Show();
            this.Hide();
        }

        private void siticoneButton4_Click(object sender, EventArgs e)
        {
            Delete d = new Delete();
            d.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
