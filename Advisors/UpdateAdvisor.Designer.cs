﻿
namespace midtermProject.Advisors
{
    partial class UpdateAdvisor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateAdvisor));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.sideBar = new System.Windows.Forms.TableLayoutPanel();
            this.MenuBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.ReportsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.EvaluationsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.GroupsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.ProjectsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.AdvisorsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.StudentsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.HomeBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Fname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Lname = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.contact = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.email = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.Gender = new System.Windows.Forms.ComboBox();
            this.siticoneButton1 = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.DesignationBox = new System.Windows.Forms.ComboBox();
            this.Salary = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.sideBar.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83F));
            this.tableLayoutPanel1.Controls.Add(this.sideBar, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1053, 676);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // sideBar
            // 
            this.sideBar.BackColor = System.Drawing.Color.Black;
            this.sideBar.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.sideBar.ColumnCount = 1;
            this.sideBar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.sideBar.Controls.Add(this.MenuBtnM, 0, 0);
            this.sideBar.Controls.Add(this.ReportsBtnM, 0, 7);
            this.sideBar.Controls.Add(this.EvaluationsBtnM, 0, 6);
            this.sideBar.Controls.Add(this.GroupsBtnM, 0, 5);
            this.sideBar.Controls.Add(this.ProjectsBtnM, 0, 4);
            this.sideBar.Controls.Add(this.AdvisorsBtnM, 0, 3);
            this.sideBar.Controls.Add(this.StudentsBtnM, 0, 2);
            this.sideBar.Controls.Add(this.HomeBtnM, 0, 1);
            this.sideBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sideBar.Location = new System.Drawing.Point(3, 3);
            this.sideBar.Name = "sideBar";
            this.sideBar.RowCount = 10;
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.6085F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.772671F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.771707F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.771707F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.920379F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.069053F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.605852F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.777969F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.752026F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.771423F));
            this.sideBar.Size = new System.Drawing.Size(173, 670);
            this.sideBar.TabIndex = 7;
            // 
            // MenuBtnM
            // 
            this.MenuBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.MenuBtnM.CheckedState.BorderColor = System.Drawing.Color.Transparent;
            this.MenuBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.MenuBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.MenuBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            this.MenuBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MenuBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.MenuBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.MenuBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.MenuBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.MenuBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.MenuBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.MenuBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MenuBtnM.FillColor = System.Drawing.Color.Black;
            this.MenuBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuBtnM.ForeColor = System.Drawing.Color.White;
            this.MenuBtnM.Image = ((System.Drawing.Image)(resources.GetObject("MenuBtnM.Image")));
            this.MenuBtnM.ImageOffset = new System.Drawing.Point(0, 10);
            this.MenuBtnM.ImageSize = new System.Drawing.Size(25, 25);
            this.MenuBtnM.Location = new System.Drawing.Point(4, 4);
            this.MenuBtnM.MaximumSize = new System.Drawing.Size(270, 698);
            this.MenuBtnM.MinimumSize = new System.Drawing.Size(74, 119);
            this.MenuBtnM.Name = "MenuBtnM";
            this.MenuBtnM.Size = new System.Drawing.Size(165, 130);
            this.MenuBtnM.TabIndex = 0;
            this.MenuBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.MenuBtnM.TextOffset = new System.Drawing.Point(15, 10);
            // 
            // ReportsBtnM
            // 
            this.ReportsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.ReportsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.ReportsBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.ReportsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.ReportsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            this.ReportsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ReportsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.ReportsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ReportsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ReportsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ReportsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ReportsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReportsBtnM.FillColor = System.Drawing.Color.Black;
            this.ReportsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportsBtnM.ForeColor = System.Drawing.Color.White;
            this.ReportsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("ReportsBtnM.Image")));
            this.ReportsBtnM.Location = new System.Drawing.Point(4, 491);
            this.ReportsBtnM.Name = "ReportsBtnM";
            this.ReportsBtnM.Size = new System.Drawing.Size(165, 51);
            this.ReportsBtnM.TabIndex = 6;
            this.ReportsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.ReportsBtnM.TextOffset = new System.Drawing.Point(25, 0);
            this.ReportsBtnM.Click += new System.EventHandler(this.ReportsBtnM_Click);
            // 
            // EvaluationsBtnM
            // 
            this.EvaluationsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.EvaluationsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.EvaluationsBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.EvaluationsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.EvaluationsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            this.EvaluationsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.EvaluationsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.EvaluationsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.EvaluationsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.EvaluationsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.EvaluationsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.EvaluationsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EvaluationsBtnM.FillColor = System.Drawing.Color.Black;
            this.EvaluationsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EvaluationsBtnM.ForeColor = System.Drawing.Color.White;
            this.EvaluationsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("EvaluationsBtnM.Image")));
            this.EvaluationsBtnM.ImageSize = new System.Drawing.Size(25, 25);
            this.EvaluationsBtnM.Location = new System.Drawing.Point(4, 434);
            this.EvaluationsBtnM.Name = "EvaluationsBtnM";
            this.EvaluationsBtnM.Size = new System.Drawing.Size(165, 50);
            this.EvaluationsBtnM.TabIndex = 5;
            this.EvaluationsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.EvaluationsBtnM.TextOffset = new System.Drawing.Point(20, 0);
            this.EvaluationsBtnM.Click += new System.EventHandler(this.EvaluationsBtnM_Click);
            // 
            // GroupsBtnM
            // 
            this.GroupsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.GroupsBtnM.CheckedState.BorderColor = System.Drawing.Color.Transparent;
            this.GroupsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.GroupsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.GroupsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            this.GroupsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GroupsBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.GroupsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.GroupsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.GroupsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.GroupsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.GroupsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.GroupsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupsBtnM.FillColor = System.Drawing.Color.Black;
            this.GroupsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupsBtnM.ForeColor = System.Drawing.Color.White;
            this.GroupsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("GroupsBtnM.Image")));
            this.GroupsBtnM.ImageSize = new System.Drawing.Size(25, 25);
            this.GroupsBtnM.Location = new System.Drawing.Point(4, 374);
            this.GroupsBtnM.Name = "GroupsBtnM";
            this.GroupsBtnM.Size = new System.Drawing.Size(165, 53);
            this.GroupsBtnM.TabIndex = 4;
            this.GroupsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.GroupsBtnM.TextOffset = new System.Drawing.Point(25, 0);
            this.GroupsBtnM.Click += new System.EventHandler(this.GroupsBtnM_Click);
            // 
            // ProjectsBtnM
            // 
            this.ProjectsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.ProjectsBtnM.CheckedState.BorderColor = System.Drawing.Color.Transparent;
            this.ProjectsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.ProjectsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.ProjectsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            this.ProjectsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ProjectsBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.ProjectsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.ProjectsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ProjectsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ProjectsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ProjectsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ProjectsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProjectsBtnM.FillColor = System.Drawing.Color.Black;
            this.ProjectsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProjectsBtnM.ForeColor = System.Drawing.Color.White;
            this.ProjectsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("ProjectsBtnM.Image")));
            this.ProjectsBtnM.ImageSize = new System.Drawing.Size(25, 25);
            this.ProjectsBtnM.Location = new System.Drawing.Point(4, 315);
            this.ProjectsBtnM.Name = "ProjectsBtnM";
            this.ProjectsBtnM.Size = new System.Drawing.Size(165, 52);
            this.ProjectsBtnM.TabIndex = 3;
            this.ProjectsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.ProjectsBtnM.TextOffset = new System.Drawing.Point(25, 0);
            this.ProjectsBtnM.Click += new System.EventHandler(this.ProjectsBtnM_Click);
            // 
            // AdvisorsBtnM
            // 
            this.AdvisorsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.AdvisorsBtnM.Checked = true;
            this.AdvisorsBtnM.CheckedState.BorderColor = System.Drawing.Color.Transparent;
            this.AdvisorsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.AdvisorsBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.AdvisorsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.AdvisorsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            this.AdvisorsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AdvisorsBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.AdvisorsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.AdvisorsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.AdvisorsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.AdvisorsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.AdvisorsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.AdvisorsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AdvisorsBtnM.FillColor = System.Drawing.Color.Black;
            this.AdvisorsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdvisorsBtnM.ForeColor = System.Drawing.Color.White;
            this.AdvisorsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("AdvisorsBtnM.Image")));
            this.AdvisorsBtnM.Location = new System.Drawing.Point(4, 257);
            this.AdvisorsBtnM.Name = "AdvisorsBtnM";
            this.AdvisorsBtnM.Size = new System.Drawing.Size(165, 51);
            this.AdvisorsBtnM.TabIndex = 2;
            this.AdvisorsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.AdvisorsBtnM.TextOffset = new System.Drawing.Point(30, 0);
            this.AdvisorsBtnM.Click += new System.EventHandler(this.AdvisorsBtnM_Click);
            // 
            // StudentsBtnM
            // 
            this.StudentsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.StudentsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.StudentsBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.StudentsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.StudentsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            this.StudentsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.StudentsBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.StudentsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.StudentsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.StudentsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.StudentsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.StudentsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.StudentsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StudentsBtnM.FillColor = System.Drawing.Color.Black;
            this.StudentsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StudentsBtnM.ForeColor = System.Drawing.Color.White;
            this.StudentsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("StudentsBtnM.Image")));
            this.StudentsBtnM.Location = new System.Drawing.Point(4, 199);
            this.StudentsBtnM.Name = "StudentsBtnM";
            this.StudentsBtnM.Size = new System.Drawing.Size(165, 51);
            this.StudentsBtnM.TabIndex = 1;
            this.StudentsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.StudentsBtnM.TextOffset = new System.Drawing.Point(30, 3);
            this.StudentsBtnM.Click += new System.EventHandler(this.StudentsBtnM_Click);
            // 
            // HomeBtnM
            // 
            this.HomeBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.HomeBtnM.CheckedState.BorderColor = System.Drawing.Color.Transparent;
            this.HomeBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.HomeBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.HomeBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.HomeBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.HomeBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.HomeBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.HomeBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.HomeBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.HomeBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.HomeBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.HomeBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HomeBtnM.FillColor = System.Drawing.Color.Black;
            this.HomeBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HomeBtnM.ForeColor = System.Drawing.Color.White;
            this.HomeBtnM.Image = ((System.Drawing.Image)(resources.GetObject("HomeBtnM.Image")));
            this.HomeBtnM.Location = new System.Drawing.Point(4, 141);
            this.HomeBtnM.Name = "HomeBtnM";
            this.HomeBtnM.Size = new System.Drawing.Size(165, 51);
            this.HomeBtnM.TabIndex = 8;
            this.HomeBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.HomeBtnM.TextOffset = new System.Drawing.Point(30, 3);
            this.HomeBtnM.Click += new System.EventHandler(this.HomeBtnM_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(182, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.97063F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80.02937F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(868, 670);
            this.tableLayoutPanel2.TabIndex = 8;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.71125F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.90931F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.88414F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.28086F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.214438F));
            this.tableLayoutPanel3.Controls.Add(this.label9, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.Fname, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label2, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.Lname, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.dateTimePicker1, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.contact, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label4, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.email, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.label6, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.Gender, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.siticoneButton1, 3, 5);
            this.tableLayoutPanel3.Controls.Add(this.DesignationBox, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.Salary, 3, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 136);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 7;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.9205F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.7364F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.5272F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.36402F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.59833F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.83254F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(862, 531);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(413, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 102);
            this.label9.TabIndex = 16;
            this.label9.Text = "Salary";
            this.label9.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 102);
            this.label7.TabIndex = 12;
            this.label7.Text = "Designation";
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 81);
            this.label1.TabIndex = 1;
            this.label1.Text = "First Name";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // Fname
            // 
            this.Fname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Fname.Location = new System.Drawing.Point(138, 158);
            this.Fname.Name = "Fname";
            this.Fname.Size = new System.Drawing.Size(269, 22);
            this.Fname.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(413, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 81);
            this.label2.TabIndex = 2;
            this.label2.Text = "Last name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // Lname
            // 
            this.Lname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Lname.Location = new System.Drawing.Point(524, 158);
            this.Lname.Name = "Lname";
            this.Lname.Size = new System.Drawing.Size(298, 22);
            this.Lname.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 263);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 84);
            this.label5.TabIndex = 8;
            this.label5.Text = "DOB";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 183);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 80);
            this.label3.TabIndex = 6;
            this.label3.Text = "Contact No";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePicker1.Location = new System.Drawing.Point(138, 322);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(269, 22);
            this.dateTimePicker1.TabIndex = 9;
            // 
            // contact
            // 
            this.contact.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.contact.Location = new System.Drawing.Point(138, 238);
            this.contact.Name = "contact";
            this.contact.Size = new System.Drawing.Size(269, 22);
            this.contact.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(413, 183);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 80);
            this.label4.TabIndex = 7;
            this.label4.Text = "Email ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // email
            // 
            this.email.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.email.Location = new System.Drawing.Point(524, 238);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(298, 22);
            this.email.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(413, 263);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 84);
            this.label6.TabIndex = 10;
            this.label6.Text = "Gender";
            this.label6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // Gender
            // 
            this.Gender.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Gender.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Gender.FormattingEnabled = true;
            this.Gender.Location = new System.Drawing.Point(524, 319);
            this.Gender.Name = "Gender";
            this.Gender.Size = new System.Drawing.Size(298, 24);
            this.Gender.TabIndex = 11;
            // 
            // siticoneButton1
            // 
            this.siticoneButton1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.siticoneButton1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.siticoneButton1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.siticoneButton1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.siticoneButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.siticoneButton1.FillColor = System.Drawing.Color.White;
            this.siticoneButton1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siticoneButton1.ForeColor = System.Drawing.Color.Black;
            this.siticoneButton1.Image = ((System.Drawing.Image)(resources.GetObject("siticoneButton1.Image")));
            this.siticoneButton1.ImageSize = new System.Drawing.Size(35, 35);
            this.siticoneButton1.Location = new System.Drawing.Point(524, 416);
            this.siticoneButton1.Name = "siticoneButton1";
            this.siticoneButton1.Size = new System.Drawing.Size(298, 66);
            this.siticoneButton1.TabIndex = 14;
            this.siticoneButton1.Text = "Update";
            this.siticoneButton1.TextOffset = new System.Drawing.Point(5, 0);
            this.siticoneButton1.Click += new System.EventHandler(this.siticoneButton1_Click);
            // 
            // DesignationBox
            // 
            this.DesignationBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DesignationBox.FormattingEnabled = true;
            this.DesignationBox.Location = new System.Drawing.Point(138, 74);
            this.DesignationBox.Name = "DesignationBox";
            this.DesignationBox.Size = new System.Drawing.Size(269, 24);
            this.DesignationBox.TabIndex = 15;
            // 
            // Salary
            // 
            this.Salary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Salary.Location = new System.Drawing.Point(524, 77);
            this.Salary.Name = "Salary";
            this.Salary.Size = new System.Drawing.Size(298, 22);
            this.Salary.TabIndex = 17;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.22857F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.51428F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.14286F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.pictureBox1, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.label8, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.65854F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.34146F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(862, 127);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(751, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(108, 62);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Black;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(143, 68);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(602, 59);
            this.label8.TabIndex = 1;
            this.label8.Text = "Update Advisor Information";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // UpdateAdvisor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1053, 676);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UpdateAdvisor";
            this.Text = "UpdateAdvisor";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.sideBar.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel sideBar;
        private Siticone.Desktop.UI.WinForms.SiticoneButton MenuBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton ReportsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton EvaluationsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton GroupsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton ProjectsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton AdvisorsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton StudentsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton HomeBtnM;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Fname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Lname;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox contact;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox Gender;
        private Siticone.Desktop.UI.WinForms.SiticoneButton siticoneButton1;
        private System.Windows.Forms.ComboBox DesignationBox;
        private System.Windows.Forms.TextBox Salary;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label8;
    }
}