﻿
namespace midtermProject
{
    partial class Advisor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Advisor));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.siticoneButton1 = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.siticoneButton2 = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.siticoneButton3 = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.siticoneButton4 = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.sideBar = new System.Windows.Forms.TableLayoutPanel();
            this.MenuBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.ReportsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.EvaluationsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.GroupsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.ProjectsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.AdvisorsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.StudentsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.HomeBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.sideBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.46679F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.53321F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.sideBar, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(968, 646);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tableLayoutPanel2.BackgroundImage")));
            this.tableLayoutPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.67966F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.64069F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.67965F));
            this.tableLayoutPanel2.Controls.Add(this.pictureBox1, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.siticoneButton1, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.siticoneButton2, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.siticoneButton3, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.siticoneButton4, 1, 8);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(288, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 10;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.0717F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.799378F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.41835F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.488336F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.57387F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.332815F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.04044F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 2.177294F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.48523F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.953343F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(677, 640);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(566, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(108, 75);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // siticoneButton1
            // 
            this.siticoneButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.siticoneButton1.CustomBorderColor = System.Drawing.Color.White;
            this.siticoneButton1.CustomBorderThickness = new System.Windows.Forms.Padding(2);
            this.siticoneButton1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.siticoneButton1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.siticoneButton1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.siticoneButton1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.siticoneButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.siticoneButton1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(71)))), ((int)(((byte)(60)))));
            this.siticoneButton1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siticoneButton1.ForeColor = System.Drawing.Color.Black;
            this.siticoneButton1.Image = ((System.Drawing.Image)(resources.GetObject("siticoneButton1.Image")));
            this.siticoneButton1.ImageOffset = new System.Drawing.Point(5, 0);
            this.siticoneButton1.ImageSize = new System.Drawing.Size(60, 60);
            this.siticoneButton1.Location = new System.Drawing.Point(115, 101);
            this.siticoneButton1.Name = "siticoneButton1";
            this.siticoneButton1.Size = new System.Drawing.Size(445, 102);
            this.siticoneButton1.TabIndex = 0;
            this.siticoneButton1.Text = "Add an Advisor";
            this.siticoneButton1.TextOffset = new System.Drawing.Point(5, 0);
            this.siticoneButton1.Click += new System.EventHandler(this.siticoneButton1_Click);
            // 
            // siticoneButton2
            // 
            this.siticoneButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.siticoneButton2.CustomBorderColor = System.Drawing.Color.White;
            this.siticoneButton2.CustomBorderThickness = new System.Windows.Forms.Padding(2);
            this.siticoneButton2.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.siticoneButton2.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.siticoneButton2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.siticoneButton2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.siticoneButton2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.siticoneButton2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(71)))), ((int)(((byte)(60)))));
            this.siticoneButton2.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siticoneButton2.ForeColor = System.Drawing.Color.Black;
            this.siticoneButton2.Image = ((System.Drawing.Image)(resources.GetObject("siticoneButton2.Image")));
            this.siticoneButton2.ImageSize = new System.Drawing.Size(60, 60);
            this.siticoneButton2.Location = new System.Drawing.Point(115, 224);
            this.siticoneButton2.Name = "siticoneButton2";
            this.siticoneButton2.Size = new System.Drawing.Size(445, 103);
            this.siticoneButton2.TabIndex = 1;
            this.siticoneButton2.Text = "View Advisors";
            this.siticoneButton2.Click += new System.EventHandler(this.siticoneButton2_Click);
            // 
            // siticoneButton3
            // 
            this.siticoneButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.siticoneButton3.CustomBorderColor = System.Drawing.Color.White;
            this.siticoneButton3.CustomBorderThickness = new System.Windows.Forms.Padding(2);
            this.siticoneButton3.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.siticoneButton3.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.siticoneButton3.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.siticoneButton3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.siticoneButton3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.siticoneButton3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(71)))), ((int)(((byte)(60)))));
            this.siticoneButton3.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siticoneButton3.ForeColor = System.Drawing.Color.Black;
            this.siticoneButton3.Image = ((System.Drawing.Image)(resources.GetObject("siticoneButton3.Image")));
            this.siticoneButton3.ImageOffset = new System.Drawing.Point(10, 0);
            this.siticoneButton3.ImageSize = new System.Drawing.Size(60, 60);
            this.siticoneButton3.Location = new System.Drawing.Point(115, 347);
            this.siticoneButton3.Name = "siticoneButton3";
            this.siticoneButton3.Size = new System.Drawing.Size(445, 106);
            this.siticoneButton3.TabIndex = 2;
            this.siticoneButton3.Text = "Update Information";
            this.siticoneButton3.TextOffset = new System.Drawing.Point(12, 0);
            this.siticoneButton3.Click += new System.EventHandler(this.siticoneButton3_Click);
            // 
            // siticoneButton4
            // 
            this.siticoneButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.siticoneButton4.CustomBorderColor = System.Drawing.Color.White;
            this.siticoneButton4.CustomBorderThickness = new System.Windows.Forms.Padding(2);
            this.siticoneButton4.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.siticoneButton4.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.siticoneButton4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.siticoneButton4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.siticoneButton4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.siticoneButton4.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(71)))), ((int)(((byte)(60)))));
            this.siticoneButton4.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siticoneButton4.ForeColor = System.Drawing.Color.Black;
            this.siticoneButton4.Image = ((System.Drawing.Image)(resources.GetObject("siticoneButton4.Image")));
            this.siticoneButton4.ImageOffset = new System.Drawing.Point(3, 0);
            this.siticoneButton4.ImageSize = new System.Drawing.Size(50, 50);
            this.siticoneButton4.Location = new System.Drawing.Point(115, 472);
            this.siticoneButton4.Name = "siticoneButton4";
            this.siticoneButton4.Size = new System.Drawing.Size(445, 97);
            this.siticoneButton4.TabIndex = 3;
            this.siticoneButton4.Text = "Delete Advisors";
            this.siticoneButton4.TextOffset = new System.Drawing.Point(10, 0);
            this.siticoneButton4.Click += new System.EventHandler(this.siticoneButton4_Click);
            // 
            // sideBar
            // 
            this.sideBar.BackColor = System.Drawing.Color.Black;
            this.sideBar.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.sideBar.ColumnCount = 1;
            this.sideBar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.sideBar.Controls.Add(this.MenuBtnM, 0, 0);
            this.sideBar.Controls.Add(this.ReportsBtnM, 0, 7);
            this.sideBar.Controls.Add(this.EvaluationsBtnM, 0, 6);
            this.sideBar.Controls.Add(this.GroupsBtnM, 0, 5);
            this.sideBar.Controls.Add(this.ProjectsBtnM, 0, 4);
            this.sideBar.Controls.Add(this.AdvisorsBtnM, 0, 3);
            this.sideBar.Controls.Add(this.StudentsBtnM, 0, 2);
            this.sideBar.Controls.Add(this.HomeBtnM, 0, 1);
            this.sideBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sideBar.Location = new System.Drawing.Point(3, 3);
            this.sideBar.Name = "sideBar";
            this.sideBar.RowCount = 9;
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.60445F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.622313F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.621255F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.621255F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.784326F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.947399F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.13298F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.066353F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.599668F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.sideBar.Size = new System.Drawing.Size(279, 640);
            this.sideBar.TabIndex = 6;
            // 
            // MenuBtnM
            // 
            this.MenuBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.MenuBtnM.CheckedState.BorderColor = System.Drawing.Color.Transparent;
            this.MenuBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.MenuBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.MenuBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.MenuBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            this.MenuBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MenuBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.MenuBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.MenuBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.MenuBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.MenuBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.MenuBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.MenuBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MenuBtnM.FillColor = System.Drawing.Color.Black;
            this.MenuBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuBtnM.ForeColor = System.Drawing.Color.White;
            this.MenuBtnM.Image = ((System.Drawing.Image)(resources.GetObject("MenuBtnM.Image")));
            this.MenuBtnM.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.MenuBtnM.ImageOffset = new System.Drawing.Point(10, 10);
            this.MenuBtnM.ImageSize = new System.Drawing.Size(25, 25);
            this.MenuBtnM.Location = new System.Drawing.Point(4, 4);
            this.MenuBtnM.MaximumSize = new System.Drawing.Size(270, 698);
            this.MenuBtnM.MinimumSize = new System.Drawing.Size(74, 119);
            this.MenuBtnM.Name = "MenuBtnM";
            this.MenuBtnM.Size = new System.Drawing.Size(270, 136);
            this.MenuBtnM.TabIndex = 0;
            this.MenuBtnM.Text = "MENU";
            this.MenuBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.MenuBtnM.TextOffset = new System.Drawing.Point(15, 10);
            // 
            // ReportsBtnM
            // 
            this.ReportsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.ReportsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.ReportsBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.ReportsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.ReportsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            this.ReportsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ReportsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.ReportsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ReportsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ReportsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ReportsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ReportsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReportsBtnM.FillColor = System.Drawing.Color.Black;
            this.ReportsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportsBtnM.ForeColor = System.Drawing.Color.White;
            this.ReportsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("ReportsBtnM.Image")));
            this.ReportsBtnM.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.ReportsBtnM.ImageOffset = new System.Drawing.Point(10, 0);
            this.ReportsBtnM.Location = new System.Drawing.Point(4, 519);
            this.ReportsBtnM.Name = "ReportsBtnM";
            this.ReportsBtnM.Size = new System.Drawing.Size(271, 51);
            this.ReportsBtnM.TabIndex = 6;
            this.ReportsBtnM.Text = "Generate Report";
            this.ReportsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.ReportsBtnM.TextOffset = new System.Drawing.Point(25, 0);
            this.ReportsBtnM.Click += new System.EventHandler(this.ReportsBtnM_Click);
            // 
            // EvaluationsBtnM
            // 
            this.EvaluationsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.EvaluationsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.EvaluationsBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.EvaluationsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.EvaluationsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            this.EvaluationsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.EvaluationsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.EvaluationsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.EvaluationsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.EvaluationsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.EvaluationsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.EvaluationsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EvaluationsBtnM.FillColor = System.Drawing.Color.Black;
            this.EvaluationsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EvaluationsBtnM.ForeColor = System.Drawing.Color.White;
            this.EvaluationsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("EvaluationsBtnM.Image")));
            this.EvaluationsBtnM.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.EvaluationsBtnM.ImageOffset = new System.Drawing.Point(10, 0);
            this.EvaluationsBtnM.ImageSize = new System.Drawing.Size(25, 25);
            this.EvaluationsBtnM.Location = new System.Drawing.Point(4, 455);
            this.EvaluationsBtnM.Name = "EvaluationsBtnM";
            this.EvaluationsBtnM.Size = new System.Drawing.Size(271, 57);
            this.EvaluationsBtnM.TabIndex = 5;
            this.EvaluationsBtnM.Text = "Evaluations Section";
            this.EvaluationsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.EvaluationsBtnM.TextOffset = new System.Drawing.Point(20, 0);
            this.EvaluationsBtnM.Click += new System.EventHandler(this.EvaluationsBtnM_Click);
            // 
            // GroupsBtnM
            // 
            this.GroupsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.GroupsBtnM.CheckedState.BorderColor = System.Drawing.Color.Transparent;
            this.GroupsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.GroupsBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.GroupsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.GroupsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            this.GroupsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GroupsBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.GroupsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.GroupsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.GroupsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.GroupsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.GroupsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.GroupsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupsBtnM.FillColor = System.Drawing.Color.Black;
            this.GroupsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupsBtnM.ForeColor = System.Drawing.Color.White;
            this.GroupsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("GroupsBtnM.Image")));
            this.GroupsBtnM.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.GroupsBtnM.ImageOffset = new System.Drawing.Point(10, 0);
            this.GroupsBtnM.ImageSize = new System.Drawing.Size(25, 25);
            this.GroupsBtnM.Location = new System.Drawing.Point(4, 392);
            this.GroupsBtnM.Name = "GroupsBtnM";
            this.GroupsBtnM.Size = new System.Drawing.Size(271, 56);
            this.GroupsBtnM.TabIndex = 4;
            this.GroupsBtnM.Text = "Groups Section";
            this.GroupsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.GroupsBtnM.TextOffset = new System.Drawing.Point(25, 0);
            this.GroupsBtnM.Click += new System.EventHandler(this.GroupsBtnM_Click);
            // 
            // ProjectsBtnM
            // 
            this.ProjectsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.ProjectsBtnM.CheckedState.BorderColor = System.Drawing.Color.Transparent;
            this.ProjectsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.ProjectsBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.ProjectsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.ProjectsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            this.ProjectsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ProjectsBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.ProjectsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.ProjectsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ProjectsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ProjectsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ProjectsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ProjectsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProjectsBtnM.FillColor = System.Drawing.Color.Black;
            this.ProjectsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProjectsBtnM.ForeColor = System.Drawing.Color.White;
            this.ProjectsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("ProjectsBtnM.Image")));
            this.ProjectsBtnM.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.ProjectsBtnM.ImageOffset = new System.Drawing.Point(10, 0);
            this.ProjectsBtnM.ImageSize = new System.Drawing.Size(25, 25);
            this.ProjectsBtnM.Location = new System.Drawing.Point(4, 330);
            this.ProjectsBtnM.Name = "ProjectsBtnM";
            this.ProjectsBtnM.Size = new System.Drawing.Size(271, 55);
            this.ProjectsBtnM.TabIndex = 3;
            this.ProjectsBtnM.Text = "Projects Section";
            this.ProjectsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.ProjectsBtnM.TextOffset = new System.Drawing.Point(25, 0);
            this.ProjectsBtnM.Click += new System.EventHandler(this.ProjectsBtnM_Click);
            // 
            // AdvisorsBtnM
            // 
            this.AdvisorsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.AdvisorsBtnM.Checked = true;
            this.AdvisorsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.AdvisorsBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.AdvisorsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.AdvisorsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            this.AdvisorsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AdvisorsBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.AdvisorsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.AdvisorsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.AdvisorsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.AdvisorsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.AdvisorsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.AdvisorsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AdvisorsBtnM.FillColor = System.Drawing.Color.Black;
            this.AdvisorsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdvisorsBtnM.ForeColor = System.Drawing.Color.White;
            this.AdvisorsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("AdvisorsBtnM.Image")));
            this.AdvisorsBtnM.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.AdvisorsBtnM.ImageOffset = new System.Drawing.Point(10, 0);
            this.AdvisorsBtnM.Location = new System.Drawing.Point(4, 269);
            this.AdvisorsBtnM.Name = "AdvisorsBtnM";
            this.AdvisorsBtnM.Size = new System.Drawing.Size(271, 54);
            this.AdvisorsBtnM.TabIndex = 2;
            this.AdvisorsBtnM.Text = "Advisors Section";
            this.AdvisorsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.AdvisorsBtnM.TextOffset = new System.Drawing.Point(30, 0);
            this.AdvisorsBtnM.Click += new System.EventHandler(this.AdvisorsBtnM_Click);
            // 
            // StudentsBtnM
            // 
            this.StudentsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.StudentsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.StudentsBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.StudentsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.StudentsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            this.StudentsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.StudentsBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.StudentsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.StudentsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.StudentsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.StudentsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.StudentsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.StudentsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StudentsBtnM.FillColor = System.Drawing.Color.Black;
            this.StudentsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StudentsBtnM.ForeColor = System.Drawing.Color.White;
            this.StudentsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("StudentsBtnM.Image")));
            this.StudentsBtnM.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.StudentsBtnM.ImageOffset = new System.Drawing.Point(10, 0);
            this.StudentsBtnM.Location = new System.Drawing.Point(4, 208);
            this.StudentsBtnM.Name = "StudentsBtnM";
            this.StudentsBtnM.Size = new System.Drawing.Size(271, 54);
            this.StudentsBtnM.TabIndex = 1;
            this.StudentsBtnM.Text = "Student Section";
            this.StudentsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.StudentsBtnM.TextOffset = new System.Drawing.Point(30, 3);
            this.StudentsBtnM.Click += new System.EventHandler(this.StudentsBtnM_Click);
            // 
            // HomeBtnM
            // 
            this.HomeBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.HomeBtnM.CheckedState.BorderColor = System.Drawing.Color.Transparent;
            this.HomeBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.HomeBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.HomeBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.HomeBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.HomeBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.HomeBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.HomeBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.HomeBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.HomeBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.HomeBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.HomeBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HomeBtnM.FillColor = System.Drawing.Color.Black;
            this.HomeBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HomeBtnM.ForeColor = System.Drawing.Color.White;
            this.HomeBtnM.Image = ((System.Drawing.Image)(resources.GetObject("HomeBtnM.Image")));
            this.HomeBtnM.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.HomeBtnM.ImageOffset = new System.Drawing.Point(10, 0);
            this.HomeBtnM.Location = new System.Drawing.Point(4, 147);
            this.HomeBtnM.Name = "HomeBtnM";
            this.HomeBtnM.Size = new System.Drawing.Size(271, 54);
            this.HomeBtnM.TabIndex = 8;
            this.HomeBtnM.Text = "Home";
            this.HomeBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.HomeBtnM.TextOffset = new System.Drawing.Point(30, 3);
            this.HomeBtnM.Click += new System.EventHandler(this.HomeBtnM_Click);
            // 
            // Advisor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 646);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Advisor";
            this.Text = "Advisor";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.sideBar.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel sideBar;
        private Siticone.Desktop.UI.WinForms.SiticoneButton MenuBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton ReportsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton EvaluationsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton GroupsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton ProjectsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton AdvisorsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton StudentsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton HomeBtnM;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Siticone.Desktop.UI.WinForms.SiticoneButton siticoneButton1;
        private Siticone.Desktop.UI.WinForms.SiticoneButton siticoneButton2;
        private Siticone.Desktop.UI.WinForms.SiticoneButton siticoneButton3;
        private Siticone.Desktop.UI.WinForms.SiticoneButton siticoneButton4;
    }
}