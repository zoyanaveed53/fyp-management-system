﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Advisors
{
    public partial class UpdateAdvisor : Form
    {
        int Id_text;

        public UpdateAdvisor()
        {
            InitializeComponent();
        }
        public UpdateAdvisor(int ID, int Salary1, int Designation, int Gender1, string FN, string LN, string Email, string Contact, string DOB)
        {
            InitializeComponent();
            DesignationBox.Items.Add("Professor");
            DesignationBox.Items.Add("Assisstant Professor");
            DesignationBox.Items.Add("Associate Professor");
            DesignationBox.Items.Add("Lecturer");
            DesignationBox.Items.Add("Industry Professional");
            Gender.Items.Add("Male");
            Gender.Items.Add("Female");

            Id_text = ID;
            Gender.SelectedIndex = Gender1 - 1;
            Fname.Text = FN;
            Lname.Text = LN;
            email.Text = Email;
            contact.Text = Contact;
            Salary.Text = Salary1.ToString();
            DesignationBox.SelectedIndex = Designation - 6;
            dateTimePicker1.Value = DateTime.Parse(DOB);
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            if (Id_text != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update Person " +
                        " set FirstName = @FirstName, LastName = @LastName, Contact = @Contact, Email = @Email, DateOfBirth = @DOB, Gender = @Gender where Id = @ID " +
                        " update Advisor set Salary = @Salary, Designation = @Designation where Id = @ID ", con);
                    cmd.Parameters.AddWithValue("@FirstName", Fname.Text);
                    cmd.Parameters.AddWithValue("@LastName", Lname.Text);
                    cmd.Parameters.AddWithValue("@Contact", contact.Text);
                    cmd.Parameters.AddWithValue("@Email", email.Text);
                    cmd.Parameters.AddWithValue("@ID", Id_text);
                    cmd.Parameters.AddWithValue("@Gender", Gender.SelectedIndex + 1);
                    cmd.Parameters.AddWithValue("@Salary", int.Parse(Salary.Text));
                    cmd.Parameters.AddWithValue("@Designation", DesignationBox.SelectedIndex + 6);
                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@DOB", dateTimePicker1.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully update");

                    Form f = new Advisor();
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {
            Evaluation E = new Evaluation();
            E.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
