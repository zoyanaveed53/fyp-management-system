﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Advisors
{
    public partial class Delete : Form
    {
        int ID;
        public Delete()
        {
            InitializeComponent();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Ad.Id , Ad.Salary, Ad.Designation , P.FirstName , P.LastName , P.Contact , P.DateOfBirth , P.Email , LU.Value as Gender \r\nfrom Advisor as Ad\r\njoin Person as P\r\n on Ad.Id = P.Id \r\n join Lookup as LU \r\non LU.Id = P.Gender");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void siticoneButton2_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {

                var con1 = Configuration.getInstance().getConnection();
                SqlCommand cmd1 = new SqlCommand("Delete from [dbo].[ProjectAdvisor] where AdvisorId = @AdvisorId", con1);
                cmd1.Parameters.AddWithValue("@AdvisorId", ID);
                cmd1.ExecuteNonQuery();

                var con4 = Configuration.getInstance().getConnection();
                SqlCommand cmd2 = new SqlCommand("Delete from Advisor where Id = @Id", con4);
                cmd2.Parameters.AddWithValue("@Id", ID);
                cmd2.ExecuteNonQuery();

                MessageBox.Show("Successfully deleted");

                Form f = new Advisor();
                f.Show();
                this.Hide();
            }

        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());

        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Ad.Id , Ad.Salary, Ad.Designation , P.FirstName , P.LastName , P.Contact , P.DateOfBirth , P.Email , LU.Value as Gender \r\nfrom Advisor as Ad\r\njoin Person as P\r\n on Ad.Id = P.Id \r\n join Lookup as LU \r\non LU.Id = P.Gender");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {
            Evaluation E = new Evaluation();
            E.Show();
            this.Hide();

        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
