﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Projects
{
    public partial class UpdateProject1 : Form
    {
        int id_text;
        public UpdateProject1()
        {
            InitializeComponent();
        }
        public UpdateProject1(int ID, string descript, string title1)
        {
            InitializeComponent();
            Description.Text = descript;
            title.Text = title1;
            id_text = ID;

        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {

            bool check = true;

            if (check == true)
            {
                if (string.IsNullOrEmpty(Description.Text) || string.IsNullOrEmpty(title.Text))
                {
                    MessageBox.Show("Fill fields");
                }
            }

            if (id_text != 0 && check == true)
            {
                try
                {

                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update Project " +
                        " set Description = @Description, Title = @Title where Id = @ID ", con);
                    cmd.Parameters.AddWithValue("@Description", Description.Text);
                    cmd.Parameters.AddWithValue("@Title", title.Text);
                    cmd.Parameters.AddWithValue("@ID", id_text);


                    cmd.ExecuteNonQuery();

                    MessageBox.Show("Successfully update");
                    Form f = new Project();
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new ViewProjects();
            a.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
