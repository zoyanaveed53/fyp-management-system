﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Projects
{
    public partial class ViewProjects : Form
    {
        public string description, title;
        public int ID;
        public ViewProjects()
        {
            InitializeComponent();

            dataGridView1.ReadOnly = true;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id, Description, Title \r\nfrom Project");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {

            dataGridView1.ReadOnly = true;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id, Description, Title \r\nfrom Project");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void siticoneButton2_Click(object sender, EventArgs e)
        {
            if (description == null || title == null)
            {
              MessageBox.Show("Select a record");
            }


            else if (ID != 0 || description != null || title != null)
            {
                try
                {
                    Form f = new UpdateProject1(ID, description, title);
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                   MessageBox.Show(error.Message);
                }

            }
        }

        private void siticoneButton3_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {
                var con1 = Configuration.getInstance().getConnection();
                SqlCommand cmd1 = new SqlCommand("Delete from [dbo].[ProjectAdvisor] where ProjectId = @ProjectId", con1);
                cmd1.Parameters.AddWithValue("@ProjectId", ID);
                cmd1.ExecuteNonQuery();


                var con3 = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Delete from GroupProject where ProjectId = @ProjectId", con3);
                cmd.Parameters.AddWithValue("@ProjectId", ID);
                cmd.ExecuteNonQuery();

                var con4 = Configuration.getInstance().getConnection();
                SqlCommand cmd2 = new SqlCommand("Delete from Project where Id = @Id", con4);
                cmd2.Parameters.AddWithValue("@Id", ID);
                cmd2.ExecuteNonQuery();

                MessageBox.Show("Successfully deleted");
                Form f = new Project();
                f.Show();
                this.Hide();
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            description = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            title = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();

        }
    }
}
