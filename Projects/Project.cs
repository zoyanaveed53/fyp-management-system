﻿using midtermProject.Evaluations;
using midtermProject.Projects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject
{
    public partial class Project : Form
    {
        public Project()
        {
            InitializeComponent();
        }

        private void siticoneCircleButton1_Click(object sender, EventArgs e)
        {

        }

        private void siticoneCircleButton2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You're Already on the page");

        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {
            Groups g = new Groups();
            g.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {
            Student s = new Student();
            s.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {
            Evaluation E = new Evaluation();
            E.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {
            Advisor A = new Advisor();
            A.Show();
            this.Hide();
;        }

        private void siticoneCircleButton1_Click_1(object sender, EventArgs e)
        {
            AddProject p = new AddProject();
            p.Show();
            this.Hide();
        }

        private void siticoneCircleButton2_Click_1(object sender, EventArgs e)
        {
            ViewProjects v = new ViewProjects();
            v.Show();
            this.Hide();
        }

        private void siticoneCircleButton3_Click(object sender, EventArgs e)
        {
        }

        private void siticoneCircleButton5_Click(object sender, EventArgs e)
        {

        }

        private void siticoneCircleButton4_Click(object sender, EventArgs e)
        {
            ProjectAdvisor p = new ProjectAdvisor();
            p.Show();
            this.Hide();
        }

        private void siticoneCircleButton6_Click(object sender, EventArgs e)
        {
            ViewProjectAdvisors p = new ViewProjectAdvisors();
            p.Show();
            this.Hide();
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
