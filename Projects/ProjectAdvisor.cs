﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Projects
{
    public partial class ProjectAdvisor : Form
    {
        public ProjectAdvisor()
        {
            InitializeComponent();
        }

        private void comboBox2_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectADatabase;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from Advisor";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Student");
                    AdvisorID.DisplayMember = "Id";
                    AdvisorID.ValueMember = "Id";
                    AdvisorID.DataSource = ds.Tables["Student"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ProjectID_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectADatabase;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from Project";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Student");
                    ProjectID.DisplayMember = "Id";
                    ProjectID.ValueMember = "Id";
                    ProjectID.DataSource = ds.Tables["Student"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            bool check = true;
            int role_advisor = 0;

            if (check == true)
            {
                if (string.IsNullOrEmpty(dateTimePicker1.Text) || string.IsNullOrEmpty(ProjectID.Text) || string.IsNullOrEmpty(AdvisorID.Text) || string.IsNullOrEmpty(Role.Text))
                {
                    check = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            try
            {
                if (check == true)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO ProjectAdvisor(AdvisorId,ProjectId,AdvisorRole,AssignmentDate) VALUES (@AdvisorId,@ProjectId, @AdvisorRole,@AssignmentDate)", con);
                    cmd.Parameters.AddWithValue("@AdvisorId", AdvisorID.SelectedValue);
                    cmd.Parameters.AddWithValue("@ProjectId", ProjectID.SelectedValue);

                    if (Role.Text == "Industry Advisor")
                    {
                        role_advisor = 14;
                        cmd.Parameters.AddWithValue("@AdvisorRole", role_advisor);

                    }
                    else if(Role.Text == "Co-Advisor")
                    {
                        role_advisor = 12;
                        cmd.Parameters.AddWithValue("@AdvisorRole", role_advisor);

                    }
                    else if (Role.Text == "Main Advisor")
                    {
                        role_advisor = 11;
                        cmd.Parameters.AddWithValue("@AdvisorRole", role_advisor);

                    }


                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@AssignmentDate", dateTimePicker1.Text);

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");

                    Form f = new Project();
                    this.Hide();
                    f.Show();
                }
            }

            catch (Exception error)
            {
                MessageBox.Show(error.Message);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void Role_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Role_Click(object sender, EventArgs e)
        {

            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectADatabase;Integrated Security=True"))
            {

                var id_advisor = AdvisorID.SelectedValue;
                var id_project = ProjectID.SelectedValue;

                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand query = new SqlCommand("select Lookup.Value from Lookup where Id > 10 except(select Lookup.Value from ProjectAdvisor join Lookup on Lookup.Id = ProjectAdvisor.AdvisorRole where ProjectId = @ProjectId) ", con);
                    query.Parameters.AddWithValue("@ProjectId", ProjectID.SelectedValue);

                    SqlDataAdapter da = new SqlDataAdapter(query);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Lookup");
                    Role.DisplayMember = "Value";
                    Role.ValueMember = "Value";
                    Role.DataSource = ds.Tables["Lookup"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}

