﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Projects
{
    public partial class ViewProjectAdvisors : Form
    {
        int advisorId, projectId;  
        string Assignmentdate, advisorRole;
        public ViewProjectAdvisors()
        {
            InitializeComponent();

            dataGridView1.ReadOnly = true;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select  ProjectAdvisor.AdvisorId, ProjectAdvisor.ProjectId, Project.Title as [Project Title] , ProjectAdvisor.AssignmentDate, Lookup.Value from ProjectAdvisor join Lookup on Lookup.Id = ProjectAdvisor.AdvisorRole join Project on ProjectAdvisor.ProjectId = Project.Id");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            advisorId = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            projectId = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
            advisorRole = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
            Assignmentdate = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        private void siticoneButton3_Click(object sender, EventArgs e)
        {
            if (advisorId != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Delete From ProjectAdvisor Where AdvisorId = @AdvisorId");
                    cmd.Parameters.AddWithValue("@AdvisorId", advisorId);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");


                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }

            else
            {
                MessageBox.Show("Select a record to delete");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            dataGridView1.ReadOnly = true;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select  ProjectAdvisor.AdvisorId, ProjectAdvisor.ProjectId, Project.Title as [Project Title] , ProjectAdvisor.AssignmentDate, Lookup.Value from ProjectAdvisor join Lookup on Lookup.Id = ProjectAdvisor.AdvisorRole join Project on ProjectAdvisor.ProjectId = Project.Id");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void siticoneButton2_Click(object sender, EventArgs e)
        {
            if (advisorId == 0 || projectId == 0)
            {
                MessageBox.Show("Select a record");
            }
            if (Assignmentdate != null && advisorId != 0 && projectId != 0 && advisorRole != null)
            {
                try
                {
                    Form f = new UpdateProjectAdvisors(advisorId, projectId, advisorRole, Assignmentdate);
                    this.Hide();
                    f.Show();
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }
    }
}
