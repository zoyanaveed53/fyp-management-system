﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Projects
{
    public partial class UpdateProjectAdvisors : Form
    {
        int id1, id2;
        public UpdateProjectAdvisors()
        {
            InitializeComponent();
        }
        public UpdateProjectAdvisors(int advisorid, int projectid, string advisorRole, string date)
        {
            InitializeComponent();
            AdvisorID.Items.Add(advisorid);
            id1 = advisorid;
            ProjectID.Items.Add(projectid);
            id2 = projectid;
            dateTimePicker1.Value = DateTime.Parse(date);
            this.dateTimePicker1.Enabled = false;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new ViewProjectAdvisors();
            a.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            if (id1 != null)
            {
                int status_option = 0;
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update ProjectAdvisor set AdvisorRole = @AdvisorRole where AdvisorId = @AdvisorId AND ProjectId = @ProjectId", con);

                    cmd.Parameters.AddWithValue("@AdvisorId", id1);
                    cmd.Parameters.AddWithValue("@ProjectId", id2);

                    if (Role.SelectedItem == "Main Advisor")
                    {
                        status_option = 11;
                    }

                    if (Role.SelectedItem == "Co-Advisor")
                    {
                        status_option = 12;
                    }
                    if (Role.SelectedItem == "Industry Advisor")
                    {
                        status_option = 14;
                    }
                    cmd.Parameters.AddWithValue("@AdvisorRole", status_option);

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully update");

                    Form f = new ViewProjectAdvisors();
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }

        }
    }
}
