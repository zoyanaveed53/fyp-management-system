﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Evaluations
{
    public partial class UpdateGroupEvaluation : Form
    {
        int id1, id2;
        public UpdateGroupEvaluation()
        {
            InitializeComponent();
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            if (id1 != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update [GroupEvaluation] " +
                        " set GroupId = @GroupId , EvaluationID = @EvaluationID, ObtainedMarks = @ObtainedMarks, EvaluationDate = @EvaluationDate where GroupId = @GroupID ", con);

                    cmd.Parameters.AddWithValue("@GroupID", id1);
                    cmd.Parameters.AddWithValue("@EvaluationID", id2);
                    cmd.Parameters.AddWithValue("@ObtainedMarks", int.Parse(marks.Text));


                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@EvaluationDate", dateTimePicker1.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully updated");

                    Form f = new ViewGroupEvaluation();
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }

        private void marks_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new ViewGroupEvaluation();
            a.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void sideBar_Paint(object sender, PaintEventArgs e)
        {
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }

        public UpdateGroupEvaluation(int group, int evaluation , int marks1, string date)
        {
            InitializeComponent();
            GroupID.Items.Add(group);
            id1 = group;
            EvaluationID.Items.Add(evaluation);
            id2 = evaluation;
            marks.Text = marks1.ToString();
            dateTimePicker1.Value = DateTime.Parse(date);
            //this.dateTimePicker1.Enabled = false;
        }
    }
}
