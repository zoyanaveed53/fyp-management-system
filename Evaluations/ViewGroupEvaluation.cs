﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Evaluations
{
    public partial class ViewGroupEvaluation : Form
    {
        int group, evaluation, marks;
        string date;
        public ViewGroupEvaluation()
        {
            InitializeComponent();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select GroupId, EvaluationId, ObtainedMarks, EvaluationDate \r\n from GroupEvaluation");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void siticoneButton3_Click(object sender, EventArgs e)
        {
            if (group != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Delete From GroupEvaluation Where GroupId = @GroupId");
                    cmd.Parameters.AddWithValue("@GroupId", group);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");

                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }

            else
            {
                MessageBox.Show("Select a record to delete");
            }
        }

        private void siticoneButton2_Click(object sender, EventArgs e)
        {
            if (group == null || evaluation == null)
            {
                MessageBox.Show("Select a record");
            }


            else if (marks != 0 || group != null || evaluation != null)
            {
                try
                {
                    Form f = new UpdateGroupEvaluation(group, evaluation, marks, date);
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }

            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            group = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            evaluation = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
            marks = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());
            date = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();

        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select GroupId, EvaluationId, ObtainedMarks, EvaluationDate \r\n from GroupEvaluation");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }
    }
}
