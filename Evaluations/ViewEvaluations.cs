﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Evaluations
{
    public partial class ViewEvaluations : Form
    {
        int id, marksText, weightageText;
        string Name;
        public ViewEvaluations()
        {
            InitializeComponent();

            dataGridView1.ReadOnly = true;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id, Name, TotalMarks, TotalWeightage \r\n from Evaluation");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            id = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            Name = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            marksText = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString());
            weightageText = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString());
        }

        private void siticoneButton3_Click(object sender, EventArgs e)
        {
            if (id != 0)
            {
                var con1 = Configuration.getInstance().getConnection();
                SqlCommand cmd1 = new SqlCommand("Delete from [dbo].[GroupEvaluation] where EvaluationId = @EvaluationId", con1);
                cmd1.Parameters.AddWithValue("@EvaluationId", id);
                cmd1.ExecuteNonQuery();

                var con4 = Configuration.getInstance().getConnection();
                SqlCommand cmd2 = new SqlCommand("Delete from Evaluation where Id = @Id", con4);
                cmd2.Parameters.AddWithValue("@Id", id);
                cmd2.ExecuteNonQuery();

                MessageBox.Show("Successfully deleted");
                Form f = new Evaluation();
                f.Show();
                this.Hide();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {

            dataGridView1.ReadOnly = true;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id, Name, TotalMarks, TotalWeightage \r\n from Evaluation");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void siticoneButton2_Click(object sender, EventArgs e)
        {
            if (Name == null || marksText == null )
            {
                MessageBox.Show("Select a record");
            }


            else if (id != 0 || marksText != null || weightageText != null)
            {
                try
                {
                    Form f = new UpdateEvaluation(id,Name,marksText,weightageText);
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }

            }
        }
    }
}
