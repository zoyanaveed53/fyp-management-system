﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Evaluations
{
    public partial class AddEvaluation : Form
    {
        public AddEvaluation()
        {
            InitializeComponent();
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            bool check = true;
            if (check == true)
            {
                if (string.IsNullOrEmpty(name.Text) || string.IsNullOrEmpty(weightage.Text) || string.IsNullOrEmpty(marks.Text))
                {
                    check = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            try
            {
                if (check == true)
                {
                    if (int.Parse(marks.Text) <= 100 && int.Parse(weightage.Text) <= 100)
                    {
                        try
                        {
                            var con = Configuration.getInstance().getConnection();
                            SqlCommand cmd = new SqlCommand("INSERT INTO Evaluation(Name,TotalMarks,TotalWeightage) VALUES (@Name,@TotalMarks,@TotalWeightage)", con);
                            cmd.Parameters.AddWithValue("@Name", name.Text);
                            cmd.Parameters.AddWithValue("@TotalMarks", marks.Text);
                            cmd.Parameters.AddWithValue("@TotalWeightage", weightage.Text);
                            cmd.ExecuteNonQuery();
                            MessageBox.Show("Successfully saved");

                            Form f = new Evaluation();
                            this.Hide();
                            f.Show();
                        }

                        catch (Exception ex)
                        {
                            MessageBox.Show("Enter valid numbers");
                        }
                    }

                    else
                    {
                        MessageBox.Show("Marks/Weightage Cannot be more than 100");
                    }

                }
            }

            catch (Exception error)
            {
                MessageBox.Show("Character input not valid");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
