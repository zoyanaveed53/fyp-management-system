﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject
{
    public partial class Evaluation : Form
    {
        public Evaluation()
        {
            InitializeComponent();
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            f.Show();
            this.Hide();
        }

        private void siticoneCircleButton1_Click(object sender, EventArgs e)
        {

            Form f = new AddEvaluation();
            f.Show();
            this.Hide();
        }

        private void siticoneCircleButton2_Click(object sender, EventArgs e)
        {

            ViewEvaluations f = new ViewEvaluations();
            f.Show();
            this.Hide();
        }

        private void siticoneCircleButton4_Click(object sender, EventArgs e)
        {
            MarkEvaluation m = new MarkEvaluation();
            m.Show();
            this.Hide();
        }

        private void siticoneCircleButton6_Click(object sender, EventArgs e)
        {
            ViewGroupEvaluation r = new ViewGroupEvaluation();
            r.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            f.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Student f = new Student();
            f.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Advisor f = new Advisor();
            f.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Project f = new Project();
            f.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Groups g = new Groups();
            g.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {
            MessageBox.Show("You are already on the page!");
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
