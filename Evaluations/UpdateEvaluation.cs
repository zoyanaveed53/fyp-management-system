﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Evaluations
{
    public partial class UpdateEvaluation : Form
    {
        int id_text;
        public UpdateEvaluation()
        {
            InitializeComponent();
        }
        public UpdateEvaluation(int id, string name1, int marks1, int weightage1)
        {
            InitializeComponent();
            id_text = id;
            marks.Text = marks1.ToString();
            name.Text = name1;
            weightage.Text = weightage1.ToString();
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {

            if (id_text != 0 && int.Parse(marks.Text) <= 100 && int.Parse(weightage.Text) <= 100)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update Evaluation " +
                        " set Name = @Name, TotalMarks = @TotalMarks, TotalWeightage = @TotalWeightage where Id = @ID ", con);

                    cmd.Parameters.AddWithValue("@ID", id_text);
                    cmd.Parameters.AddWithValue("@Name", name.Text);
                    cmd.Parameters.AddWithValue("@TotalMarks", int.Parse(marks.Text));
                    cmd.Parameters.AddWithValue("@TotalWeightage", int.Parse(weightage.Text));
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully update");

                    Form f = new ViewEvaluations();
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
            else
            {
                MessageBox.Show("Marks/Weightage Cannot be more than 100");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new ViewEvaluations();
            a.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
