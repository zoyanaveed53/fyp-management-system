﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Evaluations
{
    public partial class MarkEvaluation : Form
    {
        public MarkEvaluation()
        {
            InitializeComponent();
        }

        private void GroupID_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectADatabase;Integrated Security=True"))
            {
                try
                {
                    string query = "select GroupId from GroupStudent group by GroupId";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Student");
                    GroupID.DisplayMember = "GroupId";
                    GroupID.ValueMember = "GroupId";
                    GroupID.DataSource = ds.Tables["Student"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void EvaluationID_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectADatabase;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from Evaluation";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Student");
                    EvaluationID.DisplayMember = "Id";
                    EvaluationID.ValueMember = "Id";
                    EvaluationID.DataSource = ds.Tables["Student"];

                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            bool check = true;

            if (check == true)
            {
                if (string.IsNullOrEmpty(dateTimePicker1.Text) || string.IsNullOrEmpty(GroupID.Text) || string.IsNullOrEmpty(EvaluationID.Text) || string.IsNullOrEmpty(marks.Text))
                {
                    check = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            try
            {
                if (check == true)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO " +
                        "GroupEvaluation(GroupId,EvaluationId,ObtainedMarks,EvaluationDate) " +
                        "VALUES (@GroupId,@EvaluationId, @ObtainedMarks,@EvaluationDate)", con);
                    cmd.Parameters.AddWithValue("@GroupId", GroupID.SelectedValue);
                    cmd.Parameters.AddWithValue("@EvaluationId", EvaluationID.SelectedValue);
                    cmd.Parameters.AddWithValue("@ObtainedMarks", int.Parse(marks.Text));

                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@EvaluationDate", dateTimePicker1.Text);

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");

                    Form f = new Evaluation();
                    this.Hide();
                    f.Show();
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.ToString());
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void GroupID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}


