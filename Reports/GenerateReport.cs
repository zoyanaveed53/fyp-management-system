﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject
{
    public partial class GenerateReport : Form
    {
        public GenerateReport()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            f.Show();
            this.Hide();
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            String connection = @"Data Source=(local);Initial Catalog=ProjectADatabase;Integrated Security=True";
            string query = "select GS.StudentId as [Student ID], PA.ProjectId as [Project ID], GP.GroupId as [Group ID], PA.AdvisorId as [Advisor Id], L.Value as [Advisor Role] from GroupStudent" +
                " as GS join GroupProject as GP on GP.GroupId = GS.GroupId join ProjectAdvisor as PA on PA.ProjectId = GP.ProjectId join Lookup as L on PA.AdvisorRole = L.Id";
            PDF.GeneratePDF(connection, query);

            MessageBox.Show("PDF Report has been generated");

        }

        private void siticoneButton2_Click(object sender, EventArgs e)
        {
            String connection = @"Data Source=(local);Initial Catalog=ProjectADatabase;Integrated Security=True";
            string query = "select P.FirstName, P.LastName, L.Value as [Designation], P.Email, P.Contact , A.Salary from Advisor A join Person P on A.Id = P.Id join Lookup L on A.Designation = L.Id";
            PDF.GeneratePDF2(connection, query);

            MessageBox.Show("PDF Report has been generated");
        }

        private void siticoneButton3_Click(object sender, EventArgs e)
        {
            String connection = @"Data Source=(local);Initial Catalog=ProjectADatabase;Integrated Security=True";
            string query = "select S.RegistrationNo, P.FirstName, P.LastName, L.Value as [Gender], P.Contact, P.Email, P.DateOfBirth from Student S join Person P on S.Id = P.Id join Lookup L on P.Gender = L.Id";
            PDF.GeneratePDF3(connection, query);

            MessageBox.Show("PDF Report has been generated");
        }

        private void siticoneButton4_Click(object sender, EventArgs e)
        {
            String connection = @"Data Source=(local);Initial Catalog=ProjectADatabase;Integrated Security=True";
            string query = "  select g.Id, G.Created_On, S.RegistrationNo, GS.AssignmentDate from [Group] G join GroupStudent GS on GS.GroupId = G.Id join Student S on GS.StudentId = S.Id where GS.Status = 3";
            PDF.GeneratePDF4(connection, query);

            MessageBox.Show("PDF Report has been generated");
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {
            Form f = new Form1();
            f.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Student();
            f.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Advisor();
            f.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Project();
            f.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Groups();
            f.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Evaluation();
            f.Show();
            this.Hide();
        }

        private void MarksBtnM_Click(object sender, EventArgs e)
        {

            String connection = @"Data Source=(local);Initial Catalog=ProjectADatabase;Integrated Security=True";
            string query = "select S.RegistrationNo, PR.FirstName, PR.LastName, G.Id as [Group Id], P.Title as [Project Title], E.TotalMarks, GE.ObtainedMarks from GroupEvaluation GE join Evaluation E on GE.EvaluationId = E.Id  join [Group] G on G.Id = GE.GroupId join GroupStudent GS on GS.GroupId = G.Id join Student S on S.Id = GS.StudentId join GroupProject GP on GP.GroupId = GS.GroupId join Project P on P.Id = GP.ProjectId join Person PR on PR.Id = S.Id";
            PDF.GenerateMarksheet(connection, query);

            MessageBox.Show("PDF Report has been generated");
        }
    }
}
