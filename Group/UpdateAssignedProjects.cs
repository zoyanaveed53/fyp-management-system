﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject
{
    public partial class UpdateAssignedProjects : Form
    {
        int gid;
        public UpdateAssignedProjects()
        {
            InitializeComponent();

        }
        public UpdateAssignedProjects(int PID, int GID, string date)
        {
            InitializeComponent();

            GroupID.Items.Add(GID);
            gid = GID;
            dateTimePicker1.Value = DateTime.Parse(date);
            this.dateTimePicker1.Enabled = false;

        }

        private void PROJECTiD_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void PROJECTiD_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectADatabase;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from Project";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Project");
                    PROJECTiD.DisplayMember = "Id";
                    PROJECTiD.ValueMember = "Id";
                    PROJECTiD.DataSource = ds.Tables["Project"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void GroupID_Click(object sender, EventArgs e)
        {

        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            bool check = true;

            if (check == true)
            {
                if (string.IsNullOrEmpty(GroupID.Text) || string.IsNullOrEmpty(PROJECTiD.Text))
                {
                    MessageBox.Show("Fill fields");
                    check = false;
                }
            }

            if (check == true)
            {
                try
                {

                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update GroupProject set ProjectId = @ProjectId where GroupId = @GroupId ", con);
                    cmd.Parameters.AddWithValue("@ProjectId", PROJECTiD.SelectedValue);
                    cmd.Parameters.AddWithValue("@GroupId", gid);
                    cmd.ExecuteNonQuery();

                    MessageBox.Show("Successfully update");
                    Form f = new ViewAssignedProjects();
                    this.Hide();
                    f.Show();
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Form f = new Groups();
            f.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Form1();
            f.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Student();
            f.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Advisor();
            f.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Project();
            f.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Groups();
            f.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Evaluation();
            f.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
