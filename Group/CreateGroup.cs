﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Group
{
    public partial class CreateGroup : Form
    {
        public CreateGroup()
        {
            InitializeComponent();
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            bool flag = true;

            if (flag == true)
            {
                if (dateTimePicker1 == null)
                {
                    flag = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            if (flag == true)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO [Group](Created_On) VALUES (@date)", con);

                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@date", dateTimePicker1.Value);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully Saved");

                    Form f = new Groups();
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();

        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
