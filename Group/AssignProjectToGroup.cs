﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject
{
    public partial class AssignProjectToGroup : Form
    {
        public AssignProjectToGroup()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Form f = new Groups();
            f.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Groups();
            f.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Form1();
            f.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Student();
            f.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Advisor();
            f.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Project();
            f.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Evaluation();
            f.Show();
            this.Hide();
        }

        private void GroupID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void GroupID_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectADatabase;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from [Group]";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "[Group]");
                    GroupID.DisplayMember = "Id";
                    GroupID.ValueMember = "Id";
                    GroupID.DataSource = ds.Tables["[Group]"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void PROJECTiD_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectADatabase;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from Project";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Student");
                    PROJECTiD.DisplayMember = "Id";
                    PROJECTiD.ValueMember = "Id";
                    PROJECTiD.DataSource = ds.Tables["Student"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {            
                bool check = true;

                if (check == true)
                {
                    if (string.IsNullOrEmpty(dateTimePicker1.Text) || string.IsNullOrEmpty(PROJECTiD.Text) || string.IsNullOrEmpty(GroupID.Text))
                    {
                        check = false;
                        MessageBox.Show("Please fill all fields");
                    }
                }

                try
                {
                    if (check == true)
                    {
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand("INSERT INTO GroupProject(GroupId,ProjectId,AssignmentDate) VALUES (@GroupId,@ProjectId,@AssignmentDate)", con);
                        cmd.Parameters.AddWithValue("@GroupId", GroupID.SelectedValue);
                        cmd.Parameters.AddWithValue("@ProjectId", PROJECTiD.SelectedValue);

                        dateTimePicker1.Format = DateTimePickerFormat.Custom;
                        dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                        cmd.Parameters.AddWithValue("@AssignmentDate", dateTimePicker1.Text);

                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Successfully saved");

                        Form f = new Groups();
                        this.Hide();
                        f.Show();
                    }
                }

                catch (Exception error)
                {
                    MessageBox.Show("Project Assigned");
                }
            }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
