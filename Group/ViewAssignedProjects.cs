﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject
{
    public partial class ViewAssignedProjects : Form
    {
        int grpID, projectID;
        string date;
        public ViewAssignedProjects()
        {
            InitializeComponent();

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select ProjectId, GroupId, AssignmentDate from GroupProject");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            projectID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            grpID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
            date = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();

        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select ProjectId, GroupId, AssignmentDate from GroupProject");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Form f = new Groups();
            f.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Form1();
            f.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Student();
            f.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Advisor();
            f.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Project();
            f.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Groups();
            f.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new Evaluation();
            f.Show();
            this.Hide();
        }

        private void siticoneButton2_Click(object sender, EventArgs e)
        {
            if (projectID != 0 && grpID != 0)
            {
                Form f = new UpdateAssignedProjects(projectID, grpID, date);
                this.Hide();
                f.Show();
            }
            else
            {
                MessageBox.Show("Select a record");
            }
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }

        private void siticoneButton3_Click(object sender, EventArgs e)
        {
            if (grpID != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("delete from GroupProject where ProjectId = @ProjectId AND GroupId = @GroupId");
                    cmd.Parameters.AddWithValue("@GroupId", grpID);
                    cmd.Parameters.AddWithValue("@ProjectId", projectID);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");

                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
            else
            {
                MessageBox.Show("Select a record");
            }
        }
        }
    }

