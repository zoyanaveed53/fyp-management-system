﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Group
{
    public partial class UpdateStudentGroup : Form
    {
        int gid, sid;
        public UpdateStudentGroup()
        {
            InitializeComponent();

        }
        public UpdateStudentGroup(
        int GID , int SID , string Status,string date)
        {
            InitializeComponent();

            GroupID.Items.Add(GID);
            gid = GID;
            student.Items.Add(SID);
            sid = SID;
            dateTimePicker1.Value = DateTime.Parse(date);
            this.dateTimePicker1.Enabled = false;

        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            if (gid != null)
            {
                int status_option = 0;
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update groupStudent set Status = @status where GroupId = @GroupId AND StudentId = @StudentId", con);

                    cmd.Parameters.AddWithValue("@GroupId", gid);
                    cmd.Parameters.AddWithValue("@StudentId", sid);

                    if (Status.SelectedItem == "Active")
                    {
                        status_option = 3;
                    }

                    if (Status.SelectedItem == "InActive")
                    {
                        status_option = 4;
                    }
                    cmd.Parameters.AddWithValue("@Status", status_option);

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully update");

                    Form f = new StudentGroups();
                    this.Hide();
                    f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void Status_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new StudentGroups();
            a.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void GroupID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }

        private void UpdateStudentGroup_Load(object sender, EventArgs e)
        {

        }
    }
}
