﻿
namespace midtermProject
{
    partial class UpdateAssignedProjects
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateAssignedProjects));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.sideBar = new System.Windows.Forms.TableLayoutPanel();
            this.MenuBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.ReportsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.EvaluationsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.GroupsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.ProjectsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.AdvisorsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.StudentsBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.HomeBtnM = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.siticoneButton1 = new Siticone.Desktop.UI.WinForms.SiticoneButton();
            this.PROJECTiD = new System.Windows.Forms.ComboBox();
            this.GroupID = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.sideBar.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.125F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 86.875F));
            this.tableLayoutPanel1.Controls.Add(this.sideBar, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(939, 688);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // sideBar
            // 
            this.sideBar.BackColor = System.Drawing.Color.Black;
            this.sideBar.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.sideBar.ColumnCount = 1;
            this.sideBar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.sideBar.Controls.Add(this.MenuBtnM, 0, 0);
            this.sideBar.Controls.Add(this.ReportsBtnM, 0, 7);
            this.sideBar.Controls.Add(this.EvaluationsBtnM, 0, 6);
            this.sideBar.Controls.Add(this.GroupsBtnM, 0, 5);
            this.sideBar.Controls.Add(this.ProjectsBtnM, 0, 4);
            this.sideBar.Controls.Add(this.AdvisorsBtnM, 0, 3);
            this.sideBar.Controls.Add(this.StudentsBtnM, 0, 2);
            this.sideBar.Controls.Add(this.HomeBtnM, 0, 1);
            this.sideBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sideBar.Location = new System.Drawing.Point(3, 3);
            this.sideBar.Name = "sideBar";
            this.sideBar.RowCount = 10;
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.6085F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.772671F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.771707F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.771707F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.920379F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.069053F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.605852F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.777969F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.752026F));
            this.sideBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.771423F));
            this.sideBar.Size = new System.Drawing.Size(117, 682);
            this.sideBar.TabIndex = 6;
            // 
            // MenuBtnM
            // 
            this.MenuBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.MenuBtnM.CheckedState.BorderColor = System.Drawing.Color.Transparent;
            this.MenuBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.MenuBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.MenuBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            this.MenuBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MenuBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.MenuBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.MenuBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.MenuBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.MenuBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.MenuBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.MenuBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MenuBtnM.FillColor = System.Drawing.Color.Black;
            this.MenuBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuBtnM.ForeColor = System.Drawing.Color.White;
            this.MenuBtnM.Image = ((System.Drawing.Image)(resources.GetObject("MenuBtnM.Image")));
            this.MenuBtnM.ImageOffset = new System.Drawing.Point(0, 10);
            this.MenuBtnM.ImageSize = new System.Drawing.Size(25, 25);
            this.MenuBtnM.Location = new System.Drawing.Point(4, 4);
            this.MenuBtnM.MaximumSize = new System.Drawing.Size(270, 698);
            this.MenuBtnM.MinimumSize = new System.Drawing.Size(74, 119);
            this.MenuBtnM.Name = "MenuBtnM";
            this.MenuBtnM.Size = new System.Drawing.Size(109, 132);
            this.MenuBtnM.TabIndex = 0;
            this.MenuBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.MenuBtnM.TextOffset = new System.Drawing.Point(15, 10);
            // 
            // ReportsBtnM
            // 
            this.ReportsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.ReportsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.ReportsBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.ReportsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.ReportsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            this.ReportsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ReportsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.ReportsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ReportsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ReportsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ReportsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ReportsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ReportsBtnM.FillColor = System.Drawing.Color.Black;
            this.ReportsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReportsBtnM.ForeColor = System.Drawing.Color.White;
            this.ReportsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("ReportsBtnM.Image")));
            this.ReportsBtnM.Location = new System.Drawing.Point(4, 499);
            this.ReportsBtnM.Name = "ReportsBtnM";
            this.ReportsBtnM.Size = new System.Drawing.Size(109, 53);
            this.ReportsBtnM.TabIndex = 6;
            this.ReportsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.ReportsBtnM.TextOffset = new System.Drawing.Point(25, 0);
            this.ReportsBtnM.Click += new System.EventHandler(this.ReportsBtnM_Click);
            // 
            // EvaluationsBtnM
            // 
            this.EvaluationsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.EvaluationsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.EvaluationsBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.EvaluationsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.EvaluationsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            this.EvaluationsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.EvaluationsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.EvaluationsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.EvaluationsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.EvaluationsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.EvaluationsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.EvaluationsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EvaluationsBtnM.FillColor = System.Drawing.Color.Black;
            this.EvaluationsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EvaluationsBtnM.ForeColor = System.Drawing.Color.White;
            this.EvaluationsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("EvaluationsBtnM.Image")));
            this.EvaluationsBtnM.ImageSize = new System.Drawing.Size(25, 25);
            this.EvaluationsBtnM.Location = new System.Drawing.Point(4, 441);
            this.EvaluationsBtnM.Name = "EvaluationsBtnM";
            this.EvaluationsBtnM.Size = new System.Drawing.Size(109, 51);
            this.EvaluationsBtnM.TabIndex = 5;
            this.EvaluationsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.EvaluationsBtnM.TextOffset = new System.Drawing.Point(20, 0);
            this.EvaluationsBtnM.Click += new System.EventHandler(this.EvaluationsBtnM_Click);
            // 
            // GroupsBtnM
            // 
            this.GroupsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.GroupsBtnM.Checked = true;
            this.GroupsBtnM.CheckedState.BorderColor = System.Drawing.Color.Transparent;
            this.GroupsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.GroupsBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.GroupsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.GroupsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            this.GroupsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GroupsBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.GroupsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.GroupsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.GroupsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.GroupsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.GroupsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.GroupsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupsBtnM.FillColor = System.Drawing.Color.Black;
            this.GroupsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupsBtnM.ForeColor = System.Drawing.Color.White;
            this.GroupsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("GroupsBtnM.Image")));
            this.GroupsBtnM.ImageSize = new System.Drawing.Size(25, 25);
            this.GroupsBtnM.Location = new System.Drawing.Point(4, 380);
            this.GroupsBtnM.Name = "GroupsBtnM";
            this.GroupsBtnM.Size = new System.Drawing.Size(109, 54);
            this.GroupsBtnM.TabIndex = 4;
            this.GroupsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.GroupsBtnM.TextOffset = new System.Drawing.Point(25, 0);
            this.GroupsBtnM.Click += new System.EventHandler(this.GroupsBtnM_Click);
            // 
            // ProjectsBtnM
            // 
            this.ProjectsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.ProjectsBtnM.CheckedState.BorderColor = System.Drawing.Color.Transparent;
            this.ProjectsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.ProjectsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.ProjectsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            this.ProjectsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ProjectsBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.ProjectsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.ProjectsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ProjectsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ProjectsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ProjectsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ProjectsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProjectsBtnM.FillColor = System.Drawing.Color.Black;
            this.ProjectsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProjectsBtnM.ForeColor = System.Drawing.Color.White;
            this.ProjectsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("ProjectsBtnM.Image")));
            this.ProjectsBtnM.ImageSize = new System.Drawing.Size(25, 25);
            this.ProjectsBtnM.Location = new System.Drawing.Point(4, 320);
            this.ProjectsBtnM.Name = "ProjectsBtnM";
            this.ProjectsBtnM.Size = new System.Drawing.Size(109, 53);
            this.ProjectsBtnM.TabIndex = 3;
            this.ProjectsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.ProjectsBtnM.TextOffset = new System.Drawing.Point(25, 0);
            this.ProjectsBtnM.Click += new System.EventHandler(this.ProjectsBtnM_Click);
            // 
            // AdvisorsBtnM
            // 
            this.AdvisorsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.AdvisorsBtnM.CheckedState.BorderColor = System.Drawing.Color.Transparent;
            this.AdvisorsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.AdvisorsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.AdvisorsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            this.AdvisorsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AdvisorsBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.AdvisorsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.AdvisorsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.AdvisorsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.AdvisorsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.AdvisorsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.AdvisorsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AdvisorsBtnM.FillColor = System.Drawing.Color.Black;
            this.AdvisorsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AdvisorsBtnM.ForeColor = System.Drawing.Color.White;
            this.AdvisorsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("AdvisorsBtnM.Image")));
            this.AdvisorsBtnM.Location = new System.Drawing.Point(4, 261);
            this.AdvisorsBtnM.Name = "AdvisorsBtnM";
            this.AdvisorsBtnM.Size = new System.Drawing.Size(109, 52);
            this.AdvisorsBtnM.TabIndex = 2;
            this.AdvisorsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.AdvisorsBtnM.TextOffset = new System.Drawing.Point(30, 0);
            this.AdvisorsBtnM.Click += new System.EventHandler(this.AdvisorsBtnM_Click);
            // 
            // StudentsBtnM
            // 
            this.StudentsBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.StudentsBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.StudentsBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.StudentsBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.StudentsBtnM.CheckedState.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            this.StudentsBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.StudentsBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.StudentsBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.StudentsBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.StudentsBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.StudentsBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.StudentsBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.StudentsBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StudentsBtnM.FillColor = System.Drawing.Color.Black;
            this.StudentsBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StudentsBtnM.ForeColor = System.Drawing.Color.White;
            this.StudentsBtnM.Image = ((System.Drawing.Image)(resources.GetObject("StudentsBtnM.Image")));
            this.StudentsBtnM.Location = new System.Drawing.Point(4, 202);
            this.StudentsBtnM.Name = "StudentsBtnM";
            this.StudentsBtnM.Size = new System.Drawing.Size(109, 52);
            this.StudentsBtnM.TabIndex = 1;
            this.StudentsBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.StudentsBtnM.TextOffset = new System.Drawing.Point(30, 3);
            this.StudentsBtnM.Click += new System.EventHandler(this.StudentsBtnM_Click);
            // 
            // HomeBtnM
            // 
            this.HomeBtnM.ButtonMode = Siticone.Desktop.UI.WinForms.Enums.ButtonMode.RadioButton;
            this.HomeBtnM.CheckedState.BorderColor = System.Drawing.Color.Transparent;
            this.HomeBtnM.CheckedState.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.HomeBtnM.CheckedState.FillColor = System.Drawing.Color.Transparent;
            this.HomeBtnM.CheckedState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(62)))), ((int)(((byte)(62)))));
            this.HomeBtnM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.HomeBtnM.CustomBorderColor = System.Drawing.Color.Transparent;
            this.HomeBtnM.CustomBorderThickness = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.HomeBtnM.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.HomeBtnM.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.HomeBtnM.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.HomeBtnM.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.HomeBtnM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HomeBtnM.FillColor = System.Drawing.Color.Black;
            this.HomeBtnM.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HomeBtnM.ForeColor = System.Drawing.Color.White;
            this.HomeBtnM.Image = ((System.Drawing.Image)(resources.GetObject("HomeBtnM.Image")));
            this.HomeBtnM.Location = new System.Drawing.Point(4, 143);
            this.HomeBtnM.Name = "HomeBtnM";
            this.HomeBtnM.Size = new System.Drawing.Size(109, 52);
            this.HomeBtnM.TabIndex = 8;
            this.HomeBtnM.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.HomeBtnM.TextOffset = new System.Drawing.Point(30, 3);
            this.HomeBtnM.Click += new System.EventHandler(this.HomeBtnM_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(126, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.65679F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80.34322F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(810, 682);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.69478F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.05087F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.44311F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.078983F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.dateTimePicker1, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.siticoneButton1, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.PROJECTiD, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.GroupID, 1, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 137);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 7;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.3399F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.6601F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.620689F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.49269F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.680584F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 18.10865F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(804, 542);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(183, 131);
            this.label7.TabIndex = 12;
            this.label7.Text = "Project ID";
            this.label7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 109);
            this.label1.TabIndex = 1;
            this.label1.Text = "Group ID";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 281);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(183, 79);
            this.label5.TabIndex = 8;
            this.label5.Text = "Assignment Date";
            this.label5.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateTimePicker1.Location = new System.Drawing.Point(192, 335);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(363, 22);
            this.dateTimePicker1.TabIndex = 9;
            // 
            // siticoneButton1
            // 
            this.siticoneButton1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.siticoneButton1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.siticoneButton1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.siticoneButton1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.siticoneButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.siticoneButton1.FillColor = System.Drawing.Color.White;
            this.siticoneButton1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.siticoneButton1.ForeColor = System.Drawing.Color.Black;
            this.siticoneButton1.Image = ((System.Drawing.Image)(resources.GetObject("siticoneButton1.Image")));
            this.siticoneButton1.ImageSize = new System.Drawing.Size(35, 35);
            this.siticoneButton1.Location = new System.Drawing.Point(561, 395);
            this.siticoneButton1.Name = "siticoneButton1";
            this.siticoneButton1.Size = new System.Drawing.Size(214, 81);
            this.siticoneButton1.TabIndex = 14;
            this.siticoneButton1.Text = "Update";
            this.siticoneButton1.TextOffset = new System.Drawing.Point(5, 0);
            this.siticoneButton1.Click += new System.EventHandler(this.siticoneButton1_Click);
            // 
            // PROJECTiD
            // 
            this.PROJECTiD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PROJECTiD.BackColor = System.Drawing.Color.White;
            this.PROJECTiD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PROJECTiD.FormattingEnabled = true;
            this.PROJECTiD.Location = new System.Drawing.Point(192, 103);
            this.PROJECTiD.Name = "PROJECTiD";
            this.PROJECTiD.Size = new System.Drawing.Size(363, 24);
            this.PROJECTiD.TabIndex = 15;
            this.PROJECTiD.SelectedIndexChanged += new System.EventHandler(this.PROJECTiD_SelectedIndexChanged);
            this.PROJECTiD.Click += new System.EventHandler(this.PROJECTiD_Click);
            // 
            // GroupID
            // 
            this.GroupID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GroupID.FormattingEnabled = true;
            this.GroupID.Location = new System.Drawing.Point(192, 212);
            this.GroupID.Name = "GroupID";
            this.GroupID.Size = new System.Drawing.Size(363, 24);
            this.GroupID.TabIndex = 16;
            this.GroupID.Click += new System.EventHandler(this.GroupID_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.22857F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.51428F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.14286F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.pictureBox1, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.label8, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.65854F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.34146F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(804, 128);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(700, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(101, 62);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Black;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(133, 68);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(561, 60);
            this.label8.TabIndex = 1;
            this.label8.Text = "Update Assigned Projects Information";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UpdateAssignedProjects
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 688);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UpdateAssignedProjects";
            this.Text = "UpdateAssignedProjects";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.sideBar.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel sideBar;
        private Siticone.Desktop.UI.WinForms.SiticoneButton MenuBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton ReportsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton EvaluationsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton GroupsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton ProjectsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton AdvisorsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton StudentsBtnM;
        private Siticone.Desktop.UI.WinForms.SiticoneButton HomeBtnM;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private Siticone.Desktop.UI.WinForms.SiticoneButton siticoneButton1;
        private System.Windows.Forms.ComboBox PROJECTiD;
        private System.Windows.Forms.ComboBox GroupID;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label8;
    }
}