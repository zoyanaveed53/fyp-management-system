﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Group
{
    public partial class StudentGroups : Form
    {

        int GID = 0, SID = 0;
           string  Status ;
        string date;
        public StudentGroups()
        {
            InitializeComponent();

            dataGridView1.ReadOnly = true;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select  G.StudentId, G.GroupId, G.AssignmentDate, Lookup.Value as status from GroupStudent as G join Lookup on Lookup.Id = G.Status");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {

            dataGridView1.ReadOnly = true;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select  G.StudentId, G.GroupId, G.AssignmentDate, Lookup.Value as status from GroupStudent as G join Lookup on Lookup.Id = G.Status");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void siticoneButton3_Click(object sender, EventArgs e)
        {
            if (SID != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update GroupStudent set Status = 4 where GroupId  = @GroupId");
                    cmd.Parameters.AddWithValue("@GroupId", GID);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully deleted");
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }

            else
            {
                MessageBox.Show("Select a record to delete");
            }
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void siticoneButton2_Click(object sender, EventArgs e)
        {
            if (date == null || GID == 0 || SID == 0 )
            {
                MessageBox.Show("Select a record");
            }
            if (date != null || GID != 0 || SID != 0 || Status != null)
            {
                try
                {
                    Form f = new UpdateStudentGroup(GID, SID, Status, date);
                    this.Hide();
                    f.Show();
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            SID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            GID = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString());
            Status = (dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString());
            date = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
        }
    }
}
