﻿using midtermProject.Evaluations;
using midtermProject.Group;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject
{
    public partial class Groups : Form
    {
        public Groups()
        {
            InitializeComponent();
        }

        private void siticoneCircleButton1_Click(object sender, EventArgs e)
        {
            CreateGroup g = new CreateGroup();
            g.Show();
            this.Hide();
        }

        private void siticoneCircleButton2_Click(object sender, EventArgs e)
        {

           Form a = new ViewGroup();
            a.Show();
            this.Hide();
        }

        private void siticoneCircleButton5_Click(object sender, EventArgs e)
        {
            AddStudentInGroup s = new AddStudentInGroup();
            s.Show();
            this.Hide();
        }

        private void siticoneCircleButton4_Click(object sender, EventArgs e)
        {
            StudentGroups s = new StudentGroups();
            s.Show();
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Ypu are already on the page");
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void siticoneCircleButton3_Click(object sender, EventArgs e)
        {
            Form f = new AssignProjectToGroup();
            f.Show();
            this.Hide();
        }

        private void siticoneCircleButton6_Click(object sender, EventArgs e)
        {
            Form f = new ViewAssignedProjects();
            f.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
