﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Group
{
    public partial class UpdateGroup : Form
    {
        int ID_text;
        public UpdateGroup()
        {
            InitializeComponent();
        }
        public UpdateGroup(int ID, string DOC)
        {
            InitializeComponent();
            ID_text = ID;
            dateTimePicker1.Value = DateTime.Parse(DOC);
        }

        private void UpdateGroup_Load(object sender, EventArgs e)
        {

        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            if (ID_text != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update [Group] " +
                        " set Created_On = @Created_On where Id = @ID ", con);

                    cmd.Parameters.AddWithValue("@ID", ID_text);

                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@Created_On", dateTimePicker1.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully update");

                    //Form f = new ViewGroup();
                    //this.Hide();
                    //f.Show();
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new ViewGroup();
            a.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Student();
            a.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
