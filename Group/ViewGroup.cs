﻿using midtermProject.Group;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject
{
    public partial class ViewGroup : Form
    {

        public int ID_text;
        public string Date;
        public ViewGroup()
        {
            InitializeComponent();

            dataGridView1.ReadOnly = true;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id , Created_On \r\nfrom [Group]");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {

            dataGridView1.ReadOnly = true;

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Id , Created_On \r\nfrom [Group]");
            cmd.Connection = con;
            SqlDataReader sqlData = cmd.ExecuteReader();
            DataTable dataTable = new DataTable();
            dataTable.Load(sqlData);
            dataGridView1.DataSource = dataTable;
        }

        private void siticoneButton2_Click(object sender, EventArgs e)
        {
            if (Date == null || ID_text == 0)
            {
                MessageBox.Show("Select a record");
            }
            if (Date != null || ID_text != 0)
            {
                try
                {
                    Form f = new UpdateGroup(ID_text, Date);
                    this.Hide();
                    f.Show();
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }
        }

        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            ID_text = int.Parse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            Date = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
        }

        private void siticoneButton3_Click(object sender, EventArgs e)
        {
            if (ID_text != 0)
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("update GroupStudent set Status = 4 where GroupId = @GroupId");

                    cmd.Parameters.AddWithValue("@GroupId", ID_text);
                    cmd.Connection = con;
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully Made All the Group Members InActive");
                }

                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
            }

            else
            {
                MessageBox.Show("Select a record to delete");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Groups f = new Groups();
            f.Show();
            this.Hide();
        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form1 f = new Form1();
            f.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Student f = new Student();
            f.Show();
            this.Hide();
        }

        private void AdvisorsBtnM_Click(object sender, EventArgs e)
        {

            Advisor f = new Advisor();
            f.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Project f = new Project();
            f.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Groups f = new Groups();
            f.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Evaluation f = new Evaluation();
            f.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    }
}
