﻿using midtermProject.Evaluations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace midtermProject.Group
{
    public partial class AddStudentInGroup : Form
    {
        public AddStudentInGroup()
        {
            InitializeComponent();
            Status.Items.Add("Active");
            Status.Items.Add("InActive");

        }

        private void student_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectADatabase;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from Student";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "Student");
                    student.DisplayMember = "Id";
                    student.ValueMember = "Id";
                    student.DataSource = ds.Tables["Student"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void GroupID_Click(object sender, EventArgs e)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(local);Initial Catalog = ProjectADatabase;Integrated Security=True"))
            {
                try
                {
                    string query = "select Id from [Group]";
                    SqlDataAdapter da = new SqlDataAdapter(query, conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    da.Fill(ds, "[Group]");
                    GroupID.DisplayMember = "Id";
                    GroupID.ValueMember = "Id";
                    GroupID.DataSource = ds.Tables["[Group]"];
                }
                catch (Exception ex)
                {
                    // write exception info to log or anything else
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void siticoneButton1_Click(object sender, EventArgs e)
        {
            bool check = true;
            int status_option = -1;

            if (check == true)
            {
                if (string.IsNullOrEmpty(dateTimePicker1.Text) || string.IsNullOrEmpty(Status.Text) || string.IsNullOrEmpty(student.Text) || string.IsNullOrEmpty(GroupID.Text))
                {
                    check = false;
                    MessageBox.Show("Please fill all fields");
                }
            }

            try
            {
                if (check == true)
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO " +
                        "GroupStudent(GroupId,StudentId,Status,AssignmentDate) " +
                        "VALUES (@GroupId,@StudentId, @Status,@AssignmentDate)", con);
                    cmd.Parameters.AddWithValue("@GroupId", GroupID.SelectedValue);
                    cmd.Parameters.AddWithValue("@StudentId", student.SelectedValue);

                    if (Status.SelectedItem == "Active")
                    {
                        status_option = 3;
                    }
                    if (Status.SelectedItem == "InActive")
                    {
                        status_option = 4;
                    }

                    cmd.Parameters.AddWithValue("@Status", status_option);

                    dateTimePicker1.Format = DateTimePickerFormat.Custom;
                    dateTimePicker1.CustomFormat = "yyyy-MM-dd";
                    cmd.Parameters.AddWithValue("@AssignmentDate", dateTimePicker1.Text);

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");

                    Form f = new Groups();
                    this.Hide();
                    f.Show();
                }
            }
            catch (Exception error)
            {
                MessageBox.Show("The Student ID has already been added");
            }
        }

        private void student_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void HomeBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Form1();
            a.Show();
            this.Hide();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void StudentsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Advisor();
            a.Show();
            this.Hide();
        }

        private void ProjectsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Project();
            a.Show();
            this.Hide();
        }

        private void GroupsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Groups();
            a.Show();
            this.Hide();
        }

        private void EvaluationsBtnM_Click(object sender, EventArgs e)
        {

            Form a = new Evaluation();
            a.Show();
            this.Hide();
        }

        private void ReportsBtnM_Click(object sender, EventArgs e)
        {

            Form f = new GenerateReport();
            f.Show();
            this.Hide();
        }
    } }
    

